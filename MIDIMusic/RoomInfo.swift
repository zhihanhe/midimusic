//
//  RoomInfo.swift
//  interview
//
//  Created by zhihanhe on 16/1/7.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation

enum RoomType:String {
    case video,audio;
    
    func intType() -> String {
        switch(self) {
        case .video:
            return "video";
        case .audio:
            return "audio";
        }
    }
    
    func intToType(_ type:String) -> RoomType {
        switch(type) {
        case "video":
            return .video;
        case "audio":
            return .audio;
        default:
            return .video;
        }
    }
}

class RoomInfo:NSObject {
    var roomId:String = "";
//    var chatRoomId:String?;
    var roomType:RoomType = RoomType.video;
    var title: String = "";
    var start_time: String = "";
    
    override init() {
        super.init();
    }
    
    init(fromDictionary dic:NSDictionary) {
        super.init();
        let dictionary = dic as! [String : AnyObject];
        print("start")
        print("dic = \(dic) \r\n dictionary = \(dictionary)");
        print("end");
        setValuesForKeys(dictionary);

        let roomId = dic["id"] as? String;
        if(roomId != nil) {
            self.roomId = roomId!;
        }
        
        let roomType = dic["type"] as? String;
        if(roomType != nil) {
            //这里的方式有点怪异
            self.roomType = RoomType.video.intToType(roomType!);
        }
        
    }
    
    override func setValue(_ value: Any?, forUndefinedKey key: String) {}
    
    func toJsonString() -> String {
        return "{\"id\":\"\(roomId)\",\"name\":\"\",\"type\":\"\(roomType.intType())\"}";
    }
}

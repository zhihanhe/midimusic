//
//  User.swift
//  interview
//
//  Created by zhihanhe on 15/4/21.
//  Copyright (c) 2015年 orangelab. All rights reserved.
//

import Foundation

/*
{
email = "13800138011@xiaobanhui.com";
"email_flag" = 0;
//user_id
id = 56a9d2985475e;
//head_image
img = "http://files.xiaobanhui.com/images/default_head.png?t=1453970072";
name = "\U5728\U4e00\U8d77\U5c31";
phone = 13800138011;
"phone_flag" = 1;
*role = hr;
sex = "";
*type = personal;
}
*/

let UserSexTyleMale = "male";
let UserSexTyleFemale = "female";

//MARK: - TODO 统一数据
open class User:NSObject {
    //User id
    var id:String = "";
    var name:String = "";
    var email:String = "";
    var role:String = "";
    //var headImage:String = "";
    var headImage:String = "";
    var token:String = "";
    var sex:String = "";
    var phone:String = "";

    public override init(){
        headImage = "";
    }

    init(data:NSDictionary) {
        super.init();
        
        let id = data.object(forKey: "user_id") as? String;
        if(id != nil) {
            self.id = id!;
        }
        
        let name = data.object(forKey: "name") as? String;
        if(name != nil) {
            self.name = name!;
        }
        
        let headImage = data.object(forKey: "head_image") as? String;
        if(headImage != nil) {
            self.headImage = headImage!;
        }
        
        let email = data.object(forKey: "email") as? String;
        if(email != nil) {
            self.email = email!;
        }
        
        let sex = data.object(forKey: "sex") as? String;
        if(sex != nil) {
            self.sex = sex!;
        }
        
        let phone = data.object(forKey: "phone") as? String;
        if(phone != nil) {
            self.phone = phone!;
        }
        
    }
    
    func setToTempUser() {
        self.role = "TEMPUSER";
    }
    
    func isTempUser() -> Bool {
        return self.role == "TEMPUSER";
    }
    
    func toJsonString() -> String {
        //\"sex\":\"\(sex)\",\"phone\":\"\(phone)\"
        return "{\"user_id\":\"\(id)\",\"name\":\"\(name)\",\"head_image\":\"\(headImage)\",\"email\":\"\(email)\",\"sex\":\"\(sex)\",\"phone\":\"\(phone)\"}";
    }
}

//
//  XBHLiveViewController.swift
//  interview
//
//  Created by QiaoYijie on 16/11/1.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import UIKit
import VideoCore

///推流服务类型
enum PushFlowType {
    case normal      //普通
    case baidu       //百度
}

class XBHLiveViewController: BaseViewController, UIAlertViewDelegate {
    
    //控件
    let changeCameraButtonView: ImageButtonView = {
        let buttonView = ImageButtonView.getButtonView(title: "", image: "switchCamera")
        buttonView.isHidden = true
        return buttonView
    }()
    
    //外部数据
    var type: PushFlowType = .baidu
    var url = ""
    var roomName = ""
    
    //MARK:- sessions
    // 百度 Session
    var baiduSession: VCSimpleSession?
    
    //configuration
    //方向
    let cameraOrientation = AVCaptureVideoOrientation.portrait
    //分辨率
    let videoSize = CGSize(width: 480, height: 640)
    //码率
    let bitrate = 1024;//1024 * 125
    // 摄像头
    let cameraDevice = VCCameraState.front
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initializeSession()
        registerNotification()
        customUI()
        
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        globalView.alertView.isHidden = true
    }
    
    func customUI() {
        view.addSubview(globalView.alertView)
        view.addSubview(changeCameraButtonView)
        changeCameraButtonView.snp.makeConstraints { (make) in
            make.width.equalTo(70)
            make.height.width.equalTo(90)
            make.bottom.equalTo(view.snp.bottom).offset(10)
            make.centerX.equalTo(view.snp.centerX)
        }
        
        changeCameraButtonView.addTapGesture(self, handler: #selector(XBHLiveViewController.baiduSwitchCamera))
        title = "考试中"
    }
    
    override func back() {
        
        Utils.runInMainThread {
            let alert = UIAlertView(title: "提示",
                                    message: "确定离开考试回到交流模式吗？",
                                    delegate: self,
                                    cancelButtonTitle: "取消",
                                    otherButtonTitles: "退出");
            alert.show()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        XBHHUD.showLoading()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        baiduSession = nil
        XBHHUD.hide()
    }
    
    func initializeSession() {
        switch type {
        case .baidu:
            setUpBaiduSession()
        case .normal:
            print("normal")
        }
    }
    
    func setUpBaiduSession() {
        
        let configuration = VCSimpleSessionConfiguration()
        configuration.cameraOrientation = cameraOrientation
        configuration.videoSize = videoSize
        configuration.bitrate = UInt(bitrate)
        configuration.cameraDevice = cameraDevice
        configuration.profile = .baseline;
        configuration.audioBitrate = VCAudioBitrate.bitrate192000     //音频码率
        configuration.audioSampleRate = VCAudioSampleRate.rate44100   //音频采样率
        configuration.enableAECAndNS = false;
        
        baiduSession = VCSimpleSession(configuration: configuration)
        baiduSession?.aspectMode = VCAspectMode.fill
        baiduSession?.delegate = self
        baiduSession?.previewView.frame = self.view.bounds
        view.insertSubview(baiduSession!.previewView, at: 0)
    }
    
    //注册通知
    func registerNotification() {
        let defaultCenter:NotificationCenter = NotificationCenter.default
        //百度推流
        defaultCenter.addObserver(self, selector: #selector(XBHLiveViewController.handleRTMPStarteMessage(notification:)), name: NSNotification.Name(rawValue: RTMP_Started), object: nil);
        defaultCenter.addObserver(self, selector: #selector(XBHLiveViewController.handelRTMPErrorMessage(notification:)), name: NSNotification.Name(rawValue: RTMP_Error), object: nil);
        //程序即将进入前台
        defaultCenter.addObserver(self, selector: #selector(XBHLiveViewController.onNotificication(notification:)), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil);
        //程序即将到后台
        defaultCenter.addObserver(self, selector: #selector(XBHLiveViewController.onNotificication(notification:)), name: NSNotification.Name.UIApplicationWillResignActive, object: nil);
    }
    deinit {
        NotificationCenter.default.removeObserver(self);
    }
    
    func baiduSwitchCamera() -> Void {
        baiduSession?.switchCamera()
    }
    
    func onNotificication(notification: NSNotification) {
        baiduToggleStream()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if (buttonIndex == 0) {
            
        } else if (buttonIndex == 1) {
            
            
            
            switch type {
            case .baidu:
                baiduCloseSession()
            case .normal:
                print("normal")
            }
            
            fireOlive?.pushMessage(message: [
                "content" : "{\"role\":\"\(Config.appType.rawValue)\",\"action\":\"\(ActionType.studentStop)\"}",
                "content_type" : "msg",
                "img" : "",
                "name" : "Qiaoyijie",
                //TODO 这里的格式暂时没有定
                "time" : "\(55555555)",
                "user_id" : "\(123123)"
                ])
        }
    }
}

//百度推流相关
extension XBHLiveViewController: VCSessionDelegate {
    //推流开始
    func handleRTMPStarteMessage(notification: NSNotification) {
    
    }
    
    //捕获推流的异常事件
    func handelRTMPErrorMessage(notification: NSNotification) {
    
    }
    
    //添加了相机资源
    func didAddCameraSource(_ session: VCSimpleSession!) {
    
    }
    
    //session状态变化
    func connectionStatusChanged(_ sessionState: VCSessionState) {
        
        switch sessionState {
        case .starting:
            print("current state is starting")
        case .started:
            print("current state is Started")
            XBHHUD.hide()
            XBHHUD.showSuccess("开始考试")
        case .previewStarted:
            baiduSession?.startRtmpSession(withURL: url)
            print("current state is PreviewStarted")
            //开启美颜
            baiduSession?.enableBeautyEffect(true)
        case .ended:

            XBHHUD.showSuccess("暂停考试")
            print("current state is Ended")
            baiduSession?.enableBeautyEffect(false)
        case .error:
            print("current state is Error")
        case .none:
            print("current state is None")
        }
    }
    
    func baiduToggleStream() -> Void {
        switch baiduSession!.rtmpSessionState {
        case .none, .previewStarted, .ended, .error:
            baiduSession?.startRtmpSession(withURL: url)
        default:
            baiduSession?.endRtmpSession()
        }
    }
    
    func baiduCloseSession() -> Void {
        if baiduSession?.rtmpSessionState == .starting || baiduSession?.rtmpSessionState == .started {
            baiduSession?.endRtmpSession()
        }
    }
}


//
//  Consts.swift
//  MIDIMusic
//
//  Created by QiaoYijie on 16/11/17.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation

//工程配置

/**
 打包时还需修改
 1.bundleID
 2.bundleName
 */
struct Config {
    static let appType: AppType = .student   //App的类型
    static let urlHost = ServerType.release  //环境
}

enum AppType: String {
    case teacher
    case student
}

struct Token {
    static let orangeLabSDKID = "df3c8c6a5952c4226c902237c16745d"
}

struct ServerType {
    static let edu = "https://edu.intviu.cn"
    static let release = "https://aws_center.intviu.cn:8053"
}

struct Color {
    static let themeColor = UIColor(hex: "15a3a8")
}

struct MNotification {
    static let kRoomMessageComingNofification = "kRoomMessageComingNofification"
    static let kRoomMessageErrorNofification = "kRoomMessageErrorNofification"
}

struct BaiduConsts {
    static let playerKey = "45940388021d4897b87ffbad909e5123"
}

struct ActionType {
    static let startExam = "ACTION_START_EXAM"
    static let teacherStop = "ACTION_STOP_EXAM_BY_TEACHER"
    static let studentStop = "ACTION_STOP_EXAM_BY_STUDENT"
    static let teacherAlert = "ACTION_ALERT"
    
}
func getUdid() -> String {
    let device:UIDevice = UIDevice.current;
    return device.identifierForVendor!.uuidString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
}

struct globalView {
    static let alertView: UIView = {
        let view = UIView(frame: UIScreen.main.bounds)
        view.backgroundColor = UIColor.red
        view.alpha = 0.5
        view.isHidden = true
        return view
    }()
}

struct GlobalVar {
    static var pushUrl = ""
    static var watchUrl = ""
    static var currentRoomID = ""
}

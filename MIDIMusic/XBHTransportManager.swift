//
//  XBHTransportManager.swift
//  FireOlive_iOS
//
//  Created by QiaoYijie on 17/3/1.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

enum MessageType: String {
    case push
    case set
    case getAll
    case none
    
    static func getMessageType(type: String) -> MessageType {
        switch type {
        case "push":
            return .push
        case "set":
            return .set
        case "getAll":
            return .getAll
        default :
            return .none
        }
    }
}

class XBHTransportManager: NSObject {
    
    // 单例对象
    static let shareInstance = XBHTransportManager()
    fileprivate override init() {}
    
    
    // 不同的socket连接数组
    var transportArray = [String: XBHTransport]()
    
    // 创建一个连接
    func connect(host: String, fireOlive: XBHFireOlive) -> XBHTransport? {
        
        // 此连接是否需要创建
        var shouldCreate = true
        
        for connectHost in transportArray.keys {
            if connectHost == host {
                shouldCreate = false
            }
        }
        
        // 创建新的连接
        if shouldCreate {
            let transport = XBHTransport.init(host: host)
            transportArray[host] = transport
        }
        
        if let transport = transportArray[host] {
            transport.fireOliveArray[fireOlive.path] = fireOlive
            Print.dlog("\(transport.fireOliveArray.count)")
            return transport
        } else {
            return nil
        }
        
    }
    
}

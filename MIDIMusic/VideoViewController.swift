//
//  VideoViewController.swift
//  orangeLab_iOS
//
//  Created by zhihanhe on 16/9/8.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON

var fireOlive: XBHFireOlive?

class VideoViewController: BaseViewController,UIAlertViewDelegate, XBHFireOliveDelegate {
    
    
    var lastMessage: OliveChatMessage = OliveChatMessage(content: "", name: "", user_id: "")
    
    var jsonData: JSON = JSON(data: Data())
    var roomId:String = "";
    
    var fireOlivePath = ""
    
    var videoMeetingView: VideoMeeingView?
    
    deinit {
        fireOlive?.close()
        fireOlive = nil
        videoMeetingView?.disConnect();
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title =  "交流中"
        
        fireOlive = XBHFireOlive.init(url: "/intviu/chat/\(roomId)", baseHost: fireOlivePath)
        
        fireOlive?.delegate = self
        
        videoMeetingView = VideoMeeingView(frame: UIScreen.main.bounds)
        videoMeetingView?.jsonData = jsonData
        view.addSubview(videoMeetingView!)
        
        videoMeetingView?.pushBlock = { [weak self] in
            
//            let watchController = WatchController()
//            self?.navigationController?.pushViewController(watchController, animated: true)
            
            
            //            let liveController = XBHLiveViewController()
            //            liveController.url = "rtmp://push.bcelive.com/live/x3gwv4eeqtfy28xjb6"
            //            liveController.roomName = self?.roomId ?? ""
            //            self?.navigationController?.pushViewController(liveController, animated: true)
            
        }
        
//        videoMeetingView?.userComeInBlock = {
//            
//            NotificationCenter.default.addObserver(self, selector: #selector(VideoViewController.roomMessageComingNotification(notification:)), name: NSNotification.Name(rawValue: MNotification.kRoomMessageComingNofification), object: nil)
//        }
        
//        XBHHUD.showLoading()
//        Utils.delay(4, closure: {
//            XBHHUD.hide()
//            NotificationCenter.default.addObserver(self, selector: #selector(VideoViewController.roomMessageComingNotification(notification:)), name: NSNotification.Name(rawValue: MNotification.kRoomMessageComingNofification), object: nil)
//            
//        })
        
        
    }
    
    func fireOlivedidReceivePushMessage(message: String) {
        Print.dlog(message)
        if let dataFromString = message.data(using: .utf8, allowLossyConversion: false) {
            let messageData = JSON(data: dataFromString)
            
            Print.dlog(messageData["message"]["content"].stringValue)
            
            
            let data = messageData["message"]["content"].stringValue.data(using: .utf8, allowLossyConversion: false)
            let json = JSON(data: data ?? Data())
            
            Print.dlog("\(json["action"].stringValue)")
            
            switch json["action"].stringValue {
            case ActionType.startExam:
                print("点击了考试")
                
                Utils.runInMainThread {
                    switch Config.appType {
                    case .teacher:
                        let watchController = WatchController()
                        self.navigationController?.pushViewController(watchController, animated: true)
                    case .student:
                        let liveController = XBHLiveViewController()
                        liveController.url = GlobalVar.pushUrl;
                        liveController.roomName = self.roomId
                        self.navigationController?.pushViewController(liveController, animated: true)
                    }
                }
                
                
            case ActionType.teacherStop:
                if Config.appType == .student {
                    XBHHUD.showError("老师终止了考试")
                }
                videoMeetingView?.disConnect()
                Utils.runInMainThread {
                    _ = self.navigationController?.popViewController(animated: true)
                }
            case ActionType.studentStop:
                print("学生点击了停止")
                if Config.appType == .teacher {
                    XBHHUD.showError("学生终止了考试")
                }
                videoMeetingView?.disConnect()
                Utils.runInMainThread {
                    _ = self.navigationController?.popViewController(animated: true)
                }
                
            case ActionType.teacherAlert:
                print("teacherShowAlert")
                if Config.appType == .student {
                    globalView.alertView.isHidden = !globalView.alertView.isHidden
                }
            default: break
            }
        }
    }
    
    override func back() {
        
        let alert = UIAlertView(title: "提示", message: "是否退出考试？", delegate: self, cancelButtonTitle: "取消")
        alert.addButton(withTitle: "确定")
        alert.show()
    }
    
    func roomMessageComingNotification(notification: Notification) {
        
        if let userInfo = (notification as NSNotification).userInfo {
            if let messages = userInfo["messages"] as? [OliveChatMessage] {
                //MARK: - 消息在这里加。第一次来全部的，最后一次只有一条
//                print("()()()()()()()()()()()()()()()()()()(  =======  \( userInfo["messages"]!)")
                
                lastMessage = messages.last ?? OliveChatMessage(content: "", name: "", user_id: "");
                let str = lastMessage.content_.trimmingCharacters(in: CharacterSet.init(charactersIn: "\\"))
                let data = str.data(using: .utf8, allowLossyConversion: false)
                let json = JSON(data: data ?? Data())
                print(" +++++ \(lastMessage.content_)")
                print("\(json["action"].stringValue)")
                
                switch json["action"].stringValue {
                case ActionType.startExam:
                    print("点击了考试")
                    
                    Utils.runInMainThread {
                        switch Config.appType {
                        case .teacher:
                            let watchController = WatchController()
                            self.navigationController?.pushViewController(watchController, animated: true)
                        case .student:
                            let liveController = XBHLiveViewController()
                            liveController.url = GlobalVar.pushUrl
                            liveController.roomName = self.roomId
                            self.navigationController?.pushViewController(liveController, animated: true)
                        }
                    }
                    
                    
                case ActionType.teacherStop:
                    if Config.appType == .student {
                        XBHHUD.showError("老师终止了考试")
                    }
                    videoMeetingView?.disConnect()
                    Utils.runInMainThread {
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                case ActionType.studentStop:
                    print("学生点击了停止")
                    if Config.appType == .teacher {
                        XBHHUD.showError("学生终止了考试")
                    }
                    videoMeetingView?.disConnect()
                    Utils.runInMainThread {
                        _ = self.navigationController?.popViewController(animated: true)
                    }
                    
                case ActionType.teacherAlert:
                    print("teacherShowAlert")
                    if Config.appType == .student {
                        globalView.alertView.isHidden = !globalView.alertView.isHidden
                    }
                default: break
                }
                
            }
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        videoMeetingView?.startConnect(roomId)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        videoMeetingView?.disConnect()
    }
    
    
    func changeOperationEvent() {
        
    }
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0 {
            return
        } else {
            
            videoMeetingView?.disConnect()
            _ = navigationController?.popViewController(animated: true)
        }
    }
}

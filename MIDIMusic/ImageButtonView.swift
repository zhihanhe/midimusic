//
//  ImageButtonView.swift
//  MIDIMusic
//
//  Created by QiaoYijie on 16/11/17.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import UIKit

class ImageButtonView: UIView {

    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var backView: UIView!
    
    var isDisabled = false
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */

    override func awakeFromNib() {
        frame.size = CGSize(width: 70, height: 90)
        backView.layer.cornerRadius = 25
    }
    
    static func getButtonView(title: String, image: String) -> ImageButtonView {
        let button = ImageButtonView.loadFromNib("ImageButtonView") as! ImageButtonView
        button.icon.image = UIImage(named: image)
        button.title.text = title
        
        return button
    }
    
    func setImage(name: String) {
        icon.image = UIImage(named: name)
    }
    
    func disable() {
        isDisabled = true
        self.isUserInteractionEnabled = false
        backView.backgroundColor = UIColor.hexColor("666666")
    }
    
    func alive() {
        isDisabled = false
        self.isUserInteractionEnabled = true
        backView.backgroundColor = UIColor.hexColor("15a3a8")
    }
    
    func dialive() {
        isDisabled = true
        backView.backgroundColor = UIColor.hexColor("666666")
    }
    
}

//
//  OliveChatMessage.swift
//  interview
//
//  Created by zhihanhe on 15/8/26.
//  Copyright (c) 2015年 orangelab. All rights reserved.
//

import Foundation

enum ChatType {
    case pdf,msg;
    
    func str() -> String {
        switch self {
            case .msg :
                return "msg";
            case .pdf :
                return "pdf";
        }
    }
}

class OliveChatMessage : NSObject, JSQMessageData {
    var content_: String
    var name_: String; 
    var date_: Date
    var user_id_ :String
    var img_: String?
    var content_type_:String?
    var length_: String?
    var url_: String?
    
    convenience init(content: String?, name: String? , user_id:String?) {
        self.init(content: content, name: name, user_id: user_id, date:Date(), img: nil)
    }
    
    convenience init(content: String?, name: String? , user_id:String?, img: String?) {
        self.init(content: content, name: name, user_id: user_id, date:Date(), img: img)
    }
    
    convenience init(content: String?, name: String? , user_id:String?,content_type:String?) {
        self.init(content: content, name: name, user_id: user_id, date:Date(), img: nil)
        self.content_type_ = content_type;
    }
    
    convenience init(content: String?, name: String? , user_id:String?,date:Date,img:String?,content_type:String?) {
        self.init(content: content, name: name, user_id: user_id, date:date, img: img)
        self.content_type_ = content_type;
    }
    
    convenience init(content: String?, name: String? , user_id:String?, img: String?,content_type:String?) {
        self.init(content: content, name: name, user_id: user_id, date:Date(), img: img);
        self.content_type_ = content_type;
    }
    
    convenience init(content: String?, name: String? , user_id:String?,date:Date, img: String?,content_type:String?, length: String?, url: String?) {
        self.init(content: content, name: name, user_id: user_id, date:date, img: img);
        self.content_type_ = content_type;
        self.length_ = length;
        self.url_ = url;
        
    }
    
    init(content: String?, name: String?, user_id:String?, date:Date?, img: String?) {
        self.content_ = content!;
        self.name_ = name!
        self.user_id_ = user_id!;
        self.date_ = date!;
        self.img_ = img
    }
    
    func content() -> String! {
        return content_;
    }
    
    func name() -> String! {
        return name_;
    }
    
    func user_id() -> String! {
        return user_id_;
    }
    
    func date() -> Date! {
        return date_;
    }
    
    func img() -> String? {
        return img_;
    }
    
    func content_type() -> String {
        if(content_type_ == nil) {
            return "msg";
        }
        return content_type_!;
    }
}

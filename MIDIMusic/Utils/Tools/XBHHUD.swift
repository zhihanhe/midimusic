//
//  XBHHUD.swift
//  interview
//
//  Created by QiaoYijie on 16/8/24.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation

class XBHHUD {
    
    static var root = UIApplication.shared.keyWindow
    
    static var currentHUD = MBProgressHUD()
    
    //显示转圈
    class func showInView(_ view: UIView, mode: MBProgressHUDMode = .indeterminate) -> MBProgressHUD {
        
        currentHUD = MBProgressHUD.showAdded(to: view, animated: true)
        currentHUD.mode = mode
        currentHUD.backgroundView.style = MBProgressHUDBackgroundStyle.solidColor;
        currentHUD.backgroundView.color = UIColor(white: 0, alpha: 0.2)
        
        switch mode {
        case .indeterminate:
            currentHUD.label.text = NSLocalizedString("loading", comment: "loading - 加载中")
        default:
            currentHUD.label.text = ""
        }
        
        return currentHUD
    }
    
    //只显示文字
    class func showTextOnly(text: String) {
        root = UIApplication.shared.keyWindow
        
        let hud = MBProgressHUD.showAdded(to: root!, animated: true)
        hud.mode = .text
        hud.label.text = text
        hud.backgroundView.style = MBProgressHUDBackgroundStyle.solidColor
        hud.backgroundView.color = UIColor(white: 0.0, alpha: 0.4)
        hud.label.numberOfLines = 0
        hud.offset = CGPoint(x: 0, y: MBProgressMaxOffset)
        hud.hide(animated: true, afterDelay: 3)
    }
    
    // 显示加载中
    class func showLoading(_ title: String = NSLocalizedString("loading...", comment: "loading - 加载中")) {
        
        currentHUD.hide(animated: true)
        
        currentHUD = MBProgressHUD.showAdded(to: root!, animated: true)
        currentHUD.backgroundView.style = MBProgressHUDBackgroundStyle.solidColor
        currentHUD.backgroundView.color = UIColor(white: 0.0, alpha: 0.4)
        currentHUD.label.text = title
        currentHUD.animationType = .zoom
        currentHUD.mode = .customView
        
        let imageView = UIView()
        imageView.snp_makeConstraints { (make) in
            make.width.height.equalTo(36)
        }
        
        let layer = CAShapeLayer()
        layer.lineWidth = 2
        layer.fillColor = UIColor.clear.cgColor
        layer.strokeColor = UIColor.rgb(68, 68, 68).cgColor
        layer.frame = CGRect(x: 0, y: 0, width: 36, height: 36)
        layer.lineCap = kCALineCapRound
        
        let path = UIBezierPath(arcCenter: CGPoint(x: 18, y: 18), radius: 18, startAngle: degreesToRadians(270), endAngle: degreesToRadians(180), clockwise: true)
        layer.path = path.cgPath
        imageView.layer.addSublayer(layer)
        
        let strokeEndAnimation = CAKeyframeAnimation(keyPath: "strokeEnd")
        strokeEndAnimation.duration = 0.5
        strokeEndAnimation.values = [0.0,1]
        strokeEndAnimation.keyTimes = [0.0,1]
        
        let rotaAni = CABasicAnimation(keyPath: "transform.rotation.z")
        rotaAni.fromValue = degreesToRadians(0)
        rotaAni.toValue = degreesToRadians(720)
        rotaAni.autoreverses = true
        
        let animation = CABasicAnimation(keyPath: "strokeEnd")
        animation.toValue = UIColor.black.cgColor
        
        let group = CAAnimationGroup()
        group.repeatCount = 1000
        group.duration = 2.0
        group.animations = [strokeEndAnimation, rotaAni, animation]
        
        layer.add(group, forKey: nil)
        
        currentHUD.customView = imageView
        
        var angleFlag: Double = 1.0
        
        UIView.animate(withDuration: 1, delay: 0, options: [UIViewAnimationOptions.repeat, UIViewAnimationOptions.curveLinear], animations: {
            imageView.transform = CGAffineTransform(rotationAngle: CGFloat(M_PI / 180 * angleFlag))
            }, completion: { (flag) in
                angleFlag += 1
        })
    }
    
    //成功
    class func showSuccess(_ title: String = NSLocalizedString("Success", comment: "Success - 成功")) {
        show(title, image: "success")
    }
    
    //失败
    class func showError(_ title: String = NSLocalizedString("Error", comment: "Error - 失败")) {
        XBHHUD.hide()
        show(title, image: "error")
    }
    
    fileprivate class func show(_ title: String, image: String) {
        
        Utils.runInMainThread {
            let hud = MBProgressHUD.showAdded(to: root!, animated: true)
            hud.mode = .customView
            let imageView = UIImageView(image: UIImage(named: image))
            
            hud.bezelView
            hud.customView = imageView
            hud.backgroundView.style = MBProgressHUDBackgroundStyle.solidColor
            hud.backgroundView.color = UIColor(white: 0.0, alpha: 0.4)
            hud.label.text = title
            hud.label.numberOfLines = 0
            hud.animationType = .zoom
            
            hud.hide(animated: true, afterDelay: 2.0)
        }
        
        
    }
    
    //获取当前HUD
    fileprivate class func getCurrentHUD(_ view: UIView) -> MBProgressHUD {
        
        let hud = MBProgressHUD(for: view)
        return hud!
    }
    
    //隐藏
    class func hide() {
        DispatchQueue.main.async {
            currentHUD.hide(animated: true)
        }
    }
}

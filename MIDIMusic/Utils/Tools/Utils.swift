
//
//  Utils.swift
//  MIDIMusic
//
//  Created by QiaoYijie on 16/11/18.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation

class Utils: NSObject {
    //MARK: UserDefaults
    // 增/改
    class func defaultsSaveObject(_ value: Any?, key: String) {
        let defaults = UserDefaults.standard
        defaults.set(value, forKey: key)
        defaults.synchronize()
    }
    // 查
    class func defaultsReadObjectWithKey(_ key: String) -> Any? {
        let defaults = UserDefaults.standard
        return defaults.object(forKey: key) as Any?
    }
    // 判断是都含有
    class func defaultsHadObjectWith(_ key: String) -> Bool {
        if defaultsReadObjectWithKey(key) != nil {
            return true
        }
        return false
    }
    
    // 删
    class func defaultsRemoveObjectWithKey(_ key: String) {
        let defaults = UserDefaults.standard
        if defaultsHadObjectWith(key) {
            defaults.removeObject(forKey: key)
            defaults.synchronize()
        }
        
    }
    
    // 新线程中运行
    class func runInNewThread(_ action: @escaping () -> Void){
        DispatchQueue.global(priority: DispatchQueue.GlobalQueuePriority.default).async(execute: {
            action()
        })
    }
    
    // 主线程中运行
    class func runInMainThread(_ action: @escaping () -> Void){
        DispatchQueue.main.async(execute: {
            action()
        })
    }
    
    // 延时执行方法
    class func delay(_ delay:Double, closure:@escaping ()->()) {
        DispatchQueue.main.asyncAfter(
            deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
    }
    
    // 生成随机字符串
    class func getRandomStringOfLength(length: Int) -> String {
        
        let characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
        var ranStr = ""
        for _ in 0..<length {
            let index = Int(arc4random_uniform(UInt32(characters.characters.count)))
            ranStr.append(characters[characters.index(characters.startIndex, offsetBy: index)])
        }
        return ranStr
        
    }
    
    // 字典转换成json
    class func dicToJSONStr(_ data:NSDictionary) -> String? {
        if let dataStr = try? JSONSerialization.data(withJSONObject: data, options: []) {
            if let string = NSString(data: dataStr, encoding: String.Encoding.utf8.rawValue) as? String {
                return string;
            }
        }
        return nil;
    }
}

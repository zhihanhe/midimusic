//
//  UIView+Extension.swift
//  interview
//
//  Created by QiaoYijie on 16/7/5.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation

extension UIView {
    
    //MARK: - Frame
    
    var size: CGSize { return self.bounds.size }
    
    var width: CGFloat {
        set {
            var frame = self.frame
            frame.size.width = newValue
            self.frame = frame
        }
        get {
            return self.frame.width
        }
    }
    
    var height: CGFloat {
        set {
            var frame = self.frame
            frame.size.height = newValue
            self.frame = frame
        }
        get {
            return self.frame.height
        }
    }
    
    var minX: CGFloat { return self.frame.minX }
    
    var maxX: CGFloat { return self.frame.maxX }
    
    var midX: CGFloat { return self.frame.midX }
    
    var midY: CGFloat { return self.frame.midY }
    
    var minY: CGFloat { return self.frame.minY }
    
    var maxY: CGFloat { return self.frame.maxY }
    
    
    ///MARK: - 添加手势
    func addTapGesture(_ delegate: AnyObject, handler: Selector) -> Void{
        let tap = UITapGestureRecognizer(target: delegate, action: handler)
        addGestureRecognizer(tap)
    }
    
    //MARK: - Border
    
    /// 加圆角
    func radius(_ radius: CGFloat) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
    }
    
    /// 加圆角和边框
    func borderRadius(_ radius: CGFloat, _ width: CGFloat, _ color: UIColor) {
        self.layer.cornerRadius = radius
        self.layer.masksToBounds = true
        self.layer.borderWidth = width
        self.layer.borderColor = color.cgColor
    }
    
    /// 从xib加载一个控件
    class func loadFromNib(_ nibName: String) -> UIView? {
        
        let views = Bundle.main.loadNibNamed(nibName, owner: nil, options: nil)
        
        if let realViews = views , realViews.count > 0 {
            
            return realViews[0] as? UIView
        }
        return nil
    }
}

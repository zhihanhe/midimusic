//
//  FireOlive.swift
//  interview
//
//  Created by zhihanhe on 16/7/4.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation

//typealias callBack = ((snapshot:NSDictionary) -> Void);

class FireOlive:NSObject,SRWebSocketDelegate {
    
    fileprivate var socketHost:String = "";
    
    fileprivate var reconnectTimmer:Timer?;
    fileprivate var dbName = "";
    fileprivate var path = "";
    
    fileprivate var isFirstTime = true;
    
    fileprivate var queryHandler:((_ snapshot:NSDictionary) -> Void)?;
    
    fileprivate var sendMessageQuery:[NSDictionary] = [];
    
    fileprivate var socket:SRWebSocket?;
    
    init(url:String,baseHost host:String) {
        super.init();
        var defaultHost = host;
        if(defaultHost == "") {
            defaultHost = "wss://fireolive.intviu.cn:4700/fireolive";
        }
        self.socketHost = "\(defaultHost)?db=";
        let datas:[String] = url.split("/");
        if(datas.count < 4) {
            print("FireOlive get error url:\(url)");
            return;
        }
        dbName = datas[1];
        path = "\(datas[2])/\(datas[3])";
        
        self.sendMessageQuery.append([
            "path" : self.path,
            "action" : "getAll"]);

        reconnect();
        print("-------------------open:\(self.path)");
    }
    
    
    //TODO: zhushi
    func setValueWithAutoId(_ data:[String: Any], withCompletionBlock block:(_ error:NSError?)->Void) {
        
        let sendData = [
            "action" : "push",
            "message" : data,
            "path" : self.path
        ] as [String : Any];
        
        if self.sendMessage(sendData as NSDictionary) {
            block(nil);
            return;
        }
        
        block(NSError(domain: "fireOlive", code: 100, userInfo: nil));
        
    }
    
    
    
    func queryLimitedToLast(_ count:Int, withBlock block:@escaping (_ snapshot:NSDictionary) -> Void) {
        self.queryHandler = block;
    }
    
    func removeAllObservers() {
        //用于占位的，没有实际用途
        //print("------removeAllObservers-----")
    }
    
    func unauth() {
        reconnectTimmer?.invalidate();
        socket?.delegate = nil;
        socket?.close();
    }
    
    fileprivate func sendMessage(_ data:NSDictionary) -> Bool {
        if(socket?.readyState == SRReadyState.CLOSING
            || socket?.readyState == SRReadyState.CLOSED) {
            print("socket has closed,message:\(data); not sended...;;;\(socket?.readyState.rawValue)");
            return false;
        }
        
        if let sendString = dicToJSONStr(data) {
            
            print("sendmessagesendmessagesendmessage:\(sendString)")
            
            socket?.send(sendString);
            return true;
        }

        return false;
    }
    
    fileprivate func dicToJSONStr(_ data:NSDictionary) -> String? {
        if let dataStr = try? JSONSerialization.data(withJSONObject: data, options: []) {
            if let string = NSString(data: dataStr, encoding: String.Encoding.utf8.rawValue) as? String {
                return string;
            }
        }
        return nil;
    }
    
    @objc fileprivate func reconnect() {
        socket?.delegate = nil;
        socket = SRWebSocket(url: URL(string: "\(socketHost)\(dbName)"));
        socket?.delegate = self;
        socket?.open();
    }
    
    //MARK - SRWebSocketDelegate
    func webSocket(_ webSocket: SRWebSocket!, didReceiveMessage message: Any!) {

        print("didReceiveMessage")
        
        if let jsonDataString = message as? NSString {
            
            print("!!!!!!!!!!!!!!!!!!!!!!:\(jsonDataString)");
            
            if let messageData = jsonDataString.data(using: String.Encoding.utf8.rawValue) {
                if let jsonData = try? JSONSerialization.jsonObject(with: messageData, options: []) {
                    
                    if let jsonDic = jsonData as? NSDictionary {
                        
                        if let action = jsonDic["action"] as? String , (action == "value") {
                            
                            if let table = jsonDic["table"] as? String , (table == "live_info") {
                                var perDic: [String: AnyObject] = [:];
                                if let user_count = jsonDic["user_count"] as? Int {
                                    perDic["user_count"] = user_count as AnyObject?;
                                }
                                if let love_count = jsonDic["love_count"] as? Int {
                                    perDic["love_count"] = love_count as AnyObject?;
                                }
                                if let message = jsonDic["message"] as? NSArray {
                                    perDic["message"] = message;
                                }
                                self.queryHandler?(perDic as NSDictionary);
                                return;
                            }
                            
                            if let dic = jsonDic["message"] as? NSArray {
                                
                                if(isFirstTime) {
                                    isFirstTime = false;
                                    for message in dic {
                                        if let perDic = message as? NSDictionary {
                                            self.queryHandler?(perDic);
                                        }
                                    }
                                } else {
                                    if let last = dic.lastObject as? NSDictionary {
                                        self.queryHandler?(last);
                                    }
                                }
                                
                            } else if let dic = jsonDic["message"] as? NSDictionary {
                                self.queryHandler?(dic);
                            }
                            
                        }
                        
                    }
                    
                }
            }
        }
    }
    
    func webSocketDidOpen(_ webSocket: SRWebSocket!) {
        //网络断开再连上以后，fireolive不会主动告知网络已经断开，这边需要再连接确认下。
        if(self.sendMessageQuery.count == 0 && self.path.hasBegin("broadcast/")) {
            self.sendMessageQuery.append([
                "path" : self.path,
                "action" : "getAll"]);
        }
        print("reconnect socket...");
        while(self.sendMessageQuery.count > 0) {
            self.sendMessage(self.sendMessageQuery.popLast()!);
        }
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didFailWithError error: NSError!) {
        reconnectTimmer?.invalidate();
        reconnectTimmer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(FireOlive.reconnect), userInfo: nil, repeats: false);
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didCloseWithCode code: Int, reason: String!, wasClean: Bool) {
        reconnectTimmer?.invalidate();
        reconnectTimmer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(FireOlive.reconnect), userInfo: nil, repeats: false);
    }
}

//
//  bridge.h
//  MIDIMusic
//
//  Created by QiaoYijie on 16/11/17.
//  Copyright © 2016年 orangelab. All rights reserved.
//

#ifndef bridge_h
#define bridge_h

#import "MBProgressHUD.h"
//#import "SRWebSocket.h"
#import <orangeLab_iOS/SRWebSocket.h>


//聊天的框架
#import "JSQMessages.h"
#import "JSQMessageData.h"


//百度播放器
#import "CyberPlayerController.h"

//百度推流
//#import "VideoCore/VCSimpleSession.h"


#endif /* bridge_h */

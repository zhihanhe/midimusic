//
//  XBHBaiduPlayer.swift
//  interview
//
//  Created by QiaoYijie on 16/8/23.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation

class XBHBaiduPlayer:UIView  {
    
    var isPause = false
    
    //播放的地址
    var playUrl = ""
    
    var timer = Timer()
    
    lazy var playImage: UIImageView = {
        
        let image = UIImageView(image: UIImage(named: "broadcast_btn_pause"))
        image.frame = CGRect(x: 0, y: 0, width: 50, height: 50)
        image.center = CGPoint(x: self.width * 0.5, y: self.height * 0.5)
        image.isHidden = true
        return image
    }()
    
    lazy var speedLabel: UILabel = {
       
        let label = UILabel()
        label.textColor = Color.themeColor
        label.text = "0 KB"
        label.frame = CGRect(x: 15, y: 15, width: 100, height: 40)
        return label
    }()
    
    private lazy var playerController: CyberPlayerController = {
        print("---------------c初始化playerControllerplayerControllerplayerController！！！！！！！！！！！！！！！！！！！！")
        let controller = CyberPlayerController()
        controller.setAccessKey(BaiduConsts.playerKey)
        controller.shouldAutoplay = true
        return controller
    }()
    
    //用此方法来初始化
    convenience init(frame: CGRect, url: String) {
        self.init(frame: frame)
        self.backgroundColor = UIColor.red
        playerController.contentString = url
//        playerController.contentString = "rtmp://play.bcelive.com/live/lss-hcfusrz7ypxuk5in";
        print("---------------设置URL：：：：：：：：\(url)！！！！！！！！！！！！！！！！！！！！")
        playerController.scalingMode = CBPMovieScalingModeAspectFill
        playerController.view.frame = frame
        playerController.view.backgroundColor = UIColor.yellow
        playerController.view.frame.origin = CGPoint(x: 0, y: 0)
        addSubview(playerController.view)
        addSubview(playImage)
        XBHHUD.showLoading()
        playerController.prepareToPlay()
        
        addSubview(speedLabel)
        timer = Timer.scheduledTimer(timeInterval: 2.0, target: self, selector: #selector(XBHBaiduPlayer.timeAction), userInfo: nil, repeats: true)
    }
    
//    playController初始化完成后会调用此方法
    func prepareDone() {
        Utils.runInMainThread {
            XBHHUD.hide()
            XBHHUD.showSuccess("开始考试")
        }
        
    }
    
    func timeAction() {
        speedLabel.text = String(format: "%.0f KB/s", playerController.downloadSpeed / 1024 )
    }
    
    func stop() {
        playerController.stop()
    }
    
    //playController开始缓冲
    func didstartCaching(notification: NSNotification) {
        
        let onCaching = notification.object as? Int ?? 0
        
        if onCaching == 0 {
            Utils.runInMainThread {
                XBHHUD.hide()
            }
            return;
        }
        Utils.runInMainThread {
            XBHHUD.hide()
            XBHHUD.showLoading()
        }
    }
    
    //playController收到缓冲数据
    func didReceivingCachingData(notification: Notification) {
        
    }
    
    func didStop() {
        XBHHUD.hide()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//        NotificationCenter.defaultCenter.addObserver(self, selector: #selector(XBHBaiduPlayer.prepareDone(_:)), name: CyberPlayerLoadDidPreparedNotification, object: nil)
//        NotificationCenter.defaultCenter.addObserver(self, selector: #selector(XBHBaiduPlayer.didstartCaching(_:)), name: CyberPlayerStartCachingNotification, object: nil)
//        NotificationCenter.defaultCenter().addObserver(self, selector: #selector(XBHBaiduPlayer.didReceivingCachingData(_:)), name: CyberPlayerGotCachePercentNotification, object: nil)
        
//        NotificationCenter.default.addObserver(self, selector:#selector(XBHBaiduPlayer.prepareDone(notification:)), name: NSNotification.Name(rawValue: CyberPlayerLoadDidPreparedNotification), object: nil)
        
        print("---------------绑定事件！！！！！！！！！！！！！！！！！！！！")
        NotificationCenter.default.addObserver(self, selector: #selector(XBHBaiduPlayer.prepareDone), name:  NSNotification.Name.CyberPlayerLoadDidPrepared, object: nil)
        // addTapGesture(self, handler: #selector(XBHBaiduPlayer.pause))
        NotificationCenter.default.addObserver(self, selector: #selector(XBHBaiduPlayer.didstartCaching(notification:)), name: NSNotification.Name.CyberPlayerStartCaching, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(XBHBaiduPlayer.didReceivingCachingData(notification:)), name: NSNotification.Name.CyberPlayerGotCachePercent, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(XBHBaiduPlayer.error(notification:)), name: NSNotification.Name.CyberPlayerPlaybackError, object: nil)
    }
    
    func error(notification:NSNotification) {
        XBHHUD.hide()
        print("---！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！！---\(notification.object)");
    }
    
    //暂停播放
    func pause() {
        
        isPause = !isPause
        
        if isPause {
            playerController.pause()
            playImage.isHidden = false
        } else {
            playerController.start()
            playImage.isHidden = true
        }
    }
    
    //开始播放
    func start() {
        playerController.start()
    }
    
    deinit {
        playerController.stop()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
}

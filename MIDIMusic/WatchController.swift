//
//  WatchController.swift
//  MIDIMusic
//
//  Created by QiaoYijie on 16/11/18.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import UIKit
import MediaPlayer

class WatchController: BaseViewController,UIAlertViewDelegate {
    
    //控件
    let alert: ImageButtonView = {
        let buttonView = ImageButtonView.getButtonView(title: "", image: "alert")
        return buttonView
    }()
    
    let volum = MPVolumeView(frame: CGRect(x: 10, y: 100, width: 250, height: 30))
    
    var player: XBHBaiduPlayer?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let session = AVAudioSession.sharedInstance()
        
        do {
            try session.setCategory(AVAudioSessionCategoryPlayback, with: AVAudioSessionCategoryOptions.duckOthers)
            try session.setActive(true)
        } catch {
            
        }
        
        
        
        title = "考试中"
        let baiduPlayer = XBHBaiduPlayer(frame: CGRect(x:0, y:0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height), url: GlobalVar.watchUrl)
                view.addSubview(baiduPlayer)
        player = baiduPlayer
        
        
        view.addSubview(alert)
        alert.addTapGesture(self, handler: #selector(WatchController.showAlert))
        alert.snp.makeConstraints { (make) in
            make.width.equalTo(70)
            make.height.width.equalTo(90)
            make.bottom.equalTo(view.snp.bottom).offset(10)
            make.centerX.equalTo(view.snp.centerX)
        }
        
        volum.isHidden = true
        view.addSubview(volum)
        
        

    }
    
    func showAlert() {
        if alert.isDisabled {
            alert.alive()
        } else {
            alert.dialive()
        }
        
        fireOlive?.pushMessage(message: [
            "content" : "{\"role\":\"\(Config.appType.rawValue)\",\"action\":\"\(ActionType.teacherAlert)\"}",
            "content_type" : "msg",
            "img" : "",
            "name" : "Qiaoyijie",
            //TODO 这里的格式暂时没有定
            "time" : "\(55555555)",
            "user_id" : "\(123123)"
            ])
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        XBHHUD.hide()
    }
    
    override func back() {
        let alert = UIAlertView(title: "提示", message: "是否退出考试回到交流模式？", delegate: self, cancelButtonTitle: "取消")
        alert.addButton(withTitle: "确定")
        alert.show()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        player?.timer.invalidate()
    }
    
    func alertView(_ alertView: UIAlertView, clickedButtonAt buttonIndex: Int) {
        if buttonIndex == 0 {
            return
        } else {
                player?.stop()
            
            fireOlive?.pushMessage(message: [
                "content" : "{\"role\":\"\(Config.appType.rawValue)\",\"action\":\"\(ActionType.teacherStop)\"}",
                "content_type" : "msg",
                "img" : "",
                "name" : "Qiaoyijie",
                //TODO 这里的格式暂时没有定
                "time" : "\(55555555)",
                "user_id" : "\(123123)"
                ])
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  FireBaseChatLoader.swift
//  interview
//
//  Created by zhihanhe on 16/1/12.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation

enum RoomMessageErrorType:String {
    case receiveError = "receiveError";
    case sendError = "sendError";
    case initError = "initError";
}

//通知发来了消息。
class FireBaseChatLoader:NSObject {
    
    fileprivate var roomInfo:RoomInfo = RoomInfo();
    
    fileprivate let defaultCenter:NotificationCenter = NotificationCenter.default;
    
    internal var messages:[OliveChatMessage] = [];
    
//    var firebaseMessage: Firebase?;
    var fireoliveMessage: FireOlive?;

    var currentUser:User = User();
    
    func handleDataList(_ datas:NSDictionary) {
        
        var text = "";
        if let content = datas.object(forKey: "content") as? String {
            text = content;
        }
        var sender = "";
        if let name = datas.object(forKey: "name") as? String {
            sender = name;
        }
        var imageUrl = datas.object(forKey: "img") as? String;
        if(imageUrl == nil) {
            imageUrl = "http://files.xiaobanhui.com/images/meeting/male_0.png";
        }
        let contentType = datas.object(forKey: "content_type") as? String;
        
        let timeStr = datas.object(forKey: "time") as? NSString;
        var timeStamp:Double?;
        if(timeStr == nil) {
            timeStamp = datas.object(forKey: "time") as? Double;
        } else {
            timeStamp = timeStr?.doubleValue;
        }
        if(timeStamp == nil) {
            timeStamp = Date().timeIntervalSince1970;
        } else {
            timeStamp! /= 1000.0;
        }
        
        let userIdStr = datas.object(forKey: "user_id") as? String;
        var userId:String = "";
        if(userIdStr != nil) {
            userId = userIdStr!;
        }
        
        let lengthStr = datas.object(forKey: "length") as? Int;
        var length: String = ""
        if (lengthStr != nil) {
            length = "\(lengthStr!)";
        }
        
        let urlStr = datas.object(forKey: "url") as? String;
        var url: String = ""
        if (urlStr != nil) {
            url = urlStr!;
        }
        print("message?.length_ = \(length) url = \(url)");
        
        
        let date = Date(timeIntervalSince1970: timeStamp!);
        
        let message = OliveChatMessage(content: text, name: sender, user_id: userId, date: date, img: imageUrl, content_type: contentType, length: length, url: url)
        self.messages.append(message);
        
        self.defaultCenter.post(name: Notification.Name(rawValue: MNotification.kRoomMessageComingNofification), object: self, userInfo: ["messages":self.messages]);
    }
    
    func startPostNotification(_ roomInfo:RoomInfo,currentUser:User,fireOlivePath:String?) {
        
        if(roomInfo.roomId == self.roomInfo.roomId) {
            return;
        }
        
        clearRoomChat();
        
        self.roomInfo = roomInfo;
        self.currentUser = currentUser;
        
        if let olivePath = fireOlivePath {
//            if let chat_id = roomInfo.chatRoomId {
//                fireoliveMessage = FireOlive(url: "/intviu/chat/\(chat_id)",baseHost: olivePath);
//            } else {
//                fireoliveMessage = FireOlive(url: "/intviu/chat/\(roomInfo.roomId)",baseHost: olivePath);
//            }
            fireoliveMessage = FireOlive(url: "/intviu/chat/\(roomInfo.roomId)",baseHost: olivePath);
            fireoliveMessage?.queryLimitedToLast(25, withBlock: self.handleDataList);
        } else {
//            if let chat_id = roomInfo.chatRoomId {
//                firebaseMessage = Firebase(url: "https://intviu.firebaseio.com/chat/\(chat_id)");
//            } else {
//                firebaseMessage = Firebase(url: "https://intviu.firebaseio.com/chat/\(roomInfo.roomId)");
//            }
//            firebaseMessage = Firebase(url: "https://intviu.firebaseio.com/chat/\(roomInfo.roomId)");
//            firebaseMessage?.queryLimited(toLast: 25).observe(FEventType.childAdded, with: { (snapshot) in
//                
//                if let datas = snapshot?.value as? NSDictionary {
//                    self.handleDataList(datas);
//                }
//                
//            }, withCancel: { (error) in
//                if(error != nil) {
//                    self.defaultCenter.post(name: Notification.Name(rawValue: kRoomMessageErrorNofification), object: self, userInfo: [
//                        "type" : RoomMessageErrorType.receiveError.rawValue,
//                        "orginError" : error,
//                        "message" : "网络连接超时"
//                        ]);
//                }
//            })
        }
        
        //messagesRef!.queryLimitedToLast(25).observeEventType(FEventType.ChildAdded, withBlock: { (snapshot) in
        
//        messagesRef!.queryLimitedToLast(count:25 , withBlock : { (snapshot) in
//            
//            let datas = snapshot.value as! NSDictionary;
//            
//            var text = "";
//            if let content = datas.objectForKey("content") as? String {
//                text = content;
//            }
//            var sender = "";
//            if let name = datas.objectForKey("name") as? String {
//                sender = name;
//            }
//            var imageUrl = datas.objectForKey("img") as? String;
//            if(imageUrl == nil) {
//                imageUrl = DEFAULT_HEAD_IMAGE_PATH;
//            }
//            let contentType = datas.objectForKey("content_type") as? String;
//            
//            let timeStr = datas.objectForKey("time") as? NSString;
//            var timeStamp:Double?;
//            if(timeStr == nil) {
//                timeStamp = datas.objectForKey("time") as? Double;
//            } else {
//                timeStamp = timeStr?.doubleValue;
//            }
//            if(timeStamp == nil) {
//                timeStamp = NSDate().timeIntervalSince1970;
//            } else {
//                timeStamp! /= 1000.0;
//            }
//            
//            let userIdStr = datas.objectForKey("user_id") as? String;
//            var userId:String = "";
//            if(userIdStr != nil) {
//                userId = userIdStr!;
//            }
//            
//            let date = NSDate(timeIntervalSince1970: timeStamp!);
//            
//            let message = OliveChatMessage(content: text, name: sender, user_id: userId, date: date, img: imageUrl, content_type: contentType);
//            self.messages.append(message);
//
//            self.defaultCenter.postNotificationName(kRoomMessageComingNofification, object: self, userInfo: ["messages":self.messages]);
//        }) { (error:NSError!) -> Void in
//            if(error != nil) {
//                self.defaultCenter.postNotificationName(kRoomMessageErrorNofification, object: self, userInfo: [
//                    "type" : RoomMessageErrorType.receiveError.rawValue,
//                    "orginError" : error,
//                    "message" : "网络连接超时"
//                ]);
//            }
//        }
    }
    
    func sendMessage(_ sendData:[String:String]) {
        
//        if firebaseMessage == nil && fireoliveMessage == nil {
//            self.defaultCenter.post(name: Notification.Name(rawValue: kRoomMessageErrorNofification), object: self, userInfo: [
//                "type" : RoomMessageErrorType.initError.rawValue,
//                "orginError" : NSError(domain: "FireBaseChatLoader", code: -1, userInfo: nil),
//                "message" : NSLocalizedString("network_fail_warning", comment: "network_fail_warning - 网络异常提示")
//                ]);
//            return;
//        }
        
//        firebaseMessage?.childByAutoId().setValue(sendData,withCompletionBlock: self.handleSendDataEvent);
        
        fireoliveMessage?.setValueWithAutoId(sendData, withCompletionBlock: self.handleSendDataEvent);
        

    }
    
    func handleSendDataEvent(_ error:Error?) {
        if let err = error {
            self.defaultCenter.post(name: Notification.Name(rawValue: MNotification.kRoomMessageErrorNofification), object: self, userInfo: [
                "type" : RoomMessageErrorType.sendError.rawValue,
                "orginError" : err,
                "message" : "网络连接超时"
                ]);
        }
    }
    
    func clearRoomChat() {
        messages = [];
        roomInfo = RoomInfo();
        currentUser = User();
//        firebaseMessage?.removeAllObservers();
//        firebaseMessage?.unauth();
        fireoliveMessage?.removeAllObservers();
        fireoliveMessage?.unauth();
//        firebaseMessage = nil;
        fireoliveMessage = nil;
    }
}

//
//  LookBackViewController.swift
//  MIDIMusic
//
//  Created by QiaoYijie on 16/12/28.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import UIKit
import AVKit

class LookBackViewController: BaseViewController {
    
    var player = AVQueuePlayer()
    var layer = AVPlayerLayer()

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.black
        layer.player = player
        view.layer.addSublayer(layer)
        title = "视频回放"
        player.play()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func back() {
        _ = navigationController?.popViewController(animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  VideoMeeingView.swift
//  interview
//
//  Created by zhihanhe on 16/1/11.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import Foundation
import UIKit;
import orangeLab_iOS
import Alamofire
import SwiftyJSON

//视频目前的状态
enum VideoMettingStatus {
    //等待，还没有人接入
    case waiting,
    //连接中
    connecting
}

extension UIButton {
    /// 便利构造函数
    ///
    /// - parameter title:     title
    /// - parameter color:     color
    /// - parameter fontSize:  字体大小
    /// - parameter imageName: 图像名称
    /// - parameter backColor: 背景颜色（默认为nil）
    ///
    /// - returns: UIButton
    convenience init(title: String, fontSize: CGFloat, color: UIColor, imageName: String?, backColor: UIColor? = nil,buttonOffLeft: CGFloat = 0) {
        self.init()
        
        setTitle(title, for: .normal)
        setTitleColor(color, for: UIControlState())
        
        if let imageName = imageName {
            setImage(UIImage(named: imageName), for: UIControlState())
        }
        
        backgroundColor = backColor
        
        titleLabel?.font = UIFont.systemFont(ofSize: fontSize)
        
        self.contentVerticalAlignment = .top;
        
        sizeToFit();
        
        if let imageViewWidth = self.imageView?.frame.size.width,
            let imageViewHeight = self.imageView?.frame.size.height {
            self.titleEdgeInsets = UIEdgeInsetsMake(imageViewHeight+3,-imageViewWidth-buttonOffLeft,0,0);
        }
        
        self.frame.size = CGSize(width: self.frame.width, height: self.frame.size.height+fontSize+3);
        
        self.layer.shadowOpacity = 1.0;
        self.layer.shadowRadius = 1.0;
        self.layer.shadowColor = UIColor.black.cgColor;
        self.layer.shadowOffset = CGSize(width: 0.0, height: 0.0);
        
    }
}

extension UIImage {
    func drawInRectAspectFill(_ rect: CGRect) {
        let targetSize = rect.size
        if targetSize == CGSize.zero {
            return self.draw(in: rect)
        }
        let widthRatio    = targetSize.width  / self.size.width
        let heightRatio   = targetSize.height / self.size.height
        let scalingFactor = max(widthRatio, heightRatio)
        let newSize = CGSize(width:  self.size.width  * scalingFactor,
                             height: self.size.height * scalingFactor)
        UIGraphicsBeginImageContext(targetSize)
        let origin = CGPoint(x: (targetSize.width  - newSize.width)  / 2,
                             y: (targetSize.height - newSize.height) / 2)
        self.draw(in: CGRect(origin: origin, size: newSize))
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        scaledImage?.draw(in: rect)
    }
}

class VideoMeeingView:UIView,OLSessionDelegate {
    
    var pushBlock:(() -> Void)?
    
    var userComeInBlock:(() -> Void)?
    
    var fireOlivePath = ""
    
    //private let SMALL_VIDEO_WIDTH:CGFloat = 120;
    //private let SMALL_VIDEO_HEIGHT:CGFloat = 120;
    
    var operationButtons:[UIButton] = [];
    var operationButtons_n: [ImageButtonView] = [];
    
    //mediaServerData
    var jsonData:JSON = JSON(data: Data())
    
    //在别的控制器上面显示时
    fileprivate let MINI_VIDEO_WIDTH:CGFloat = 120;
    fileprivate let MINI_VIDEO_HEIGHT:CGFloat = 160;
    
    //private let SMALL_VIDEO_WIDTH_MINI:CGFloat = 45;
    //private let SMALL_VIDEO_HEIGHT_MINI:CGFloat = 60;
    
    
    var currentStatus:VideoMettingStatus = .waiting;
    
    //直接调用的方法
    fileprivate var roomId:String = "";
    fileprivate var user:OLConnectorInfos?;
    
    fileprivate var videoUsers:[OLConnectorInfos] = [];

    //默认显示下面的bar
    fileprivate var isBarShowing = true;
    
    weak var publisher:OLPublisher?;
    
    //状态标志位
    //静音的标志位
    //var enableVoice:Bool = true;
    //视频显示的标志位
    var videoVisible:Bool = true;
    //前置摄像头的标志位
    var fontCamera:Bool = true;
    
    var userHeadImgView: UIImageView?;
    
    let voiceButton:UIButton = UIButton(title: "静音",fontSize: 12,color: UIColor.white, imageName: "mute_off",backColor: nil,buttonOffLeft: 10);
    let videoButton:UIButton = UIButton(title: "视频",fontSize: 12,color: UIColor.white, imageName: "camera_on",backColor: nil,buttonOffLeft: 10);
    let cameraChangeButton:UIButton = UIButton(title: "切换摄像头",fontSize: 12,color: UIColor.white, imageName: "switchCamera",backColor: nil,buttonOffLeft: 10);
    
    let voiceButton_n = ImageButtonView.getButtonView(title: "", image: "mute_off")
    let videoButton_n = ImageButtonView.getButtonView(title: "", image: "camera_on")
    let cameraChangeButton_n = ImageButtonView.getButtonView(title: "", image: "switchCamera")
    let examButton_n = ImageButtonView.getButtonView(title: "", image: "exam")
    
    //    let beautifulChangeButton:UIButton = UIButton(imageName: "mettingCamAfter", backImageName: nil);
    
    //视频被显示成一个一个的正方形，最多4个
    var minVideoSize:CGSize = CGSize.zero;
    
    var session:OLSession?;
    
    deinit {
        session = nil;
        print("*************************VideoMeeingView deinit***********************************");
    }
    
    override func didMoveToSuperview() {
        super.didMoveToSuperview();
        
        
        if Config.appType == .teacher {
            
            let defaultLabel = UILabel(frame: UIScreen.main.bounds)
            defaultLabel.text = "请等待学生进入"
            defaultLabel.frame.origin.y -= 64
            defaultLabel.textAlignment = .center
            defaultLabel.numberOfLines = 0
            defaultLabel.textColor = UIColor.white
            self.addSubview(defaultLabel)
        }
        
        registerNotification();
        if(self.superview == nil) {
            return;
        }
        examButton_n.disable()
        //获取当前的最大正方形
        let bounds = UIScreen.main.bounds;
        
        //取出最小的边
        //let minSize = bounds.width > bounds.height ? bounds.height : bounds.width;
        //minVideoSize = CGSizeMake(minSize/2, minSize/2);
        minVideoSize = CGSize(width: bounds.width/2, height: bounds.height/2);
        
        operationButtons = [voiceButton,videoButton,cameraChangeButton/*,beautifulChangeButton*/];
        operationButtons_n = [examButton_n, voiceButton_n, videoButton_n, cameraChangeButton_n]
        voiceButton.addTarget(self, action: #selector(VideoMeeingView.setSlienceAction(_:)), for: UIControlEvents.touchUpInside);
        voiceButton_n.addTapGesture(self, handler: #selector(VideoMeeingView.setSlienceAction_n))
        
        videoButton.addTarget(self, action: #selector(VideoMeeingView.videoDisableAction(_:)), for: UIControlEvents.touchUpInside);
        videoButton_n.addTapGesture(self, handler: #selector(VideoMeeingView.videoDisAbleAction_n))
        cameraChangeButton.addTarget(self, action: #selector(VideoMeeingView.camerChangeAction(_:)), for: UIControlEvents.touchUpInside);
        cameraChangeButton_n.addTapGesture(self, handler: #selector(VideoMeeingView.changeCamera_n))
        
        examButton_n.addTapGesture(self, handler: #selector(VideoMeeingView.enterExam))
        
        
        //        beautifulChangeButton.addTarget(self, action: #selector(VideoMeeingView.beautifulChangeAction(_:)), forControlEvents: UIControlEvents.TouchUpInside);
        
    }
    
//    func handleNotificationDisConnect() {
//        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: kOliveCloseMettingNotification), object: nil);
//        print("处理关闭连接")
//        session?.bye(callback: {
//            self.session?.disConnect();
//        });
//    }
    
    //防止事件被重复绑定
    override func removeFromSuperview() {
        super.removeFromSuperview();
        voiceButton.removeTarget(self, action: #selector(VideoMeeingView.setSlienceAction(_:)), for: UIControlEvents.touchUpInside);
        videoButton.removeTarget(self, action: #selector(VideoMeeingView.videoDisableAction(_:)), for: UIControlEvents.touchUpInside);
        cameraChangeButton.removeTarget(self, action: #selector(VideoMeeingView.camerChangeAction(_:)), for: UIControlEvents.touchUpInside);
        //        beautifulChangeButton.removeTarget(self, action: #selector(VideoMeeingView.camerChangeAction(_:)), forControlEvents: UIControlEvents.TouchUpInside);
        
        NotificationCenter.default.removeObserver(self);
    }
    
    // MARK: 绑定通知
    func registerNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(VideoMeeingView.publisherMoveUp), name: NSNotification.Name(rawValue: kHideVideoInfoViewNotification), object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(VideoMeeingView.publisherMoveDown), name: NSNotification.Name(rawValue: kShowVideoInfoViewNotification), object: nil);
        NotificationCenter.default.addObserver(self, selector: #selector(VideoMeeingView.publisherRotate), name: NSNotification.Name.UIDeviceOrientationDidChange, object: nil);
    }

    func publisherRotate() {
        publisher?.refreshVideoRotate();
        
    }
    
    //集体静音
    func muteAll() {
        session?.muteAll();
    }
    
    //MARK: - 绑定的事件
    func camerChangeAction(_ sender: UIButton) {
        
        fontCamera = !fontCamera;
        session?.changeCamera();
    }
    
    func changeCamera_n() {
        
        fontCamera = !fontCamera;
        session?.changeCamera();
    }
    
    //进入直播
    func enterExam() {
        
        if let action = self.pushBlock {
            action()
        }
        
        fireOlive?.pushMessage(message: [
            "content" : "{\"role\":\"\(Config.appType.rawValue)\",\"action\":\"\(ActionType.startExam)\"}",
            "content_type" : "msg",
            "img" : "",
            "name" : "Qiaoyijie",
            //TODO 这里的格式暂时没有定
            "time" : "\(55555555)",
            "user_id" : "\(123123)"
            ])
        
        
    }
    
    //处理静音还是有声音
    func setSlienceAction(_ sender: UIButton) {
        
        //enableVoice = !enableVoice;
        changeSlienceButtonType();
        session?.changeSoundType(!publisher!.isMute);
    }
    
    func setSlienceAction_n() {
        
        changeSlienceButtonType();
        session?.changeSoundType(!publisher!.isMute);
    }
    
    func changeSlienceButtonType() {
        if(!publisher!.isMute) {
            //静音
            voiceButton.setImage(UIImage(named: "mute_on"), for: UIControlState());
            voiceButton_n.setImage(name: "mute_off")
        } else {
            //非静音
            voiceButton.setImage(UIImage(named: "mute_off"), for: UIControlState());
            voiceButton_n.setImage(name: "mute_on")
        }
    }
    
    func videoDisableAction(_ sender: UIButton) {
        
        videoVisible = !videoVisible;
        if(!videoVisible) {
            //输出视频
            sender.setImage(UIImage(named: "camera_off"), for: UIControlState());
            videoButton_n.setImage(name: "camera_off")
            self.userHeadImgView!.frame = (self.publisher?.view.frame)!;
            self.addSubview(userHeadImgView!);
            bringButtonsToTop();
        } else {
            //关闭视频
            sender.setImage(UIImage(named: "camera_on"), for: UIControlState());
            videoButton_n.setImage(name: "camera_on")
            self.userHeadImgView?.removeFromSuperview();
        }
        session?.disableCamera(videoVisible, andStopCapture: true);
    }
    
    func videoDisAbleAction_n() {
        videoVisible = !videoVisible;
        if(!videoVisible) {
            //输出视频
            videoButton_n.setImage(name: "camera_off")
            self.userHeadImgView!.frame = (self.publisher?.view.frame)!;
            self.addSubview(userHeadImgView!);
            bringButtonsToTop();
        } else {
            //关闭视频
            videoButton_n.setImage(name: "camera_on")
            self.userHeadImgView?.removeFromSuperview();
        }
        session?.disableCamera(videoVisible, andStopCapture: true);
    }
    
    //    func beautifulChangeAction(sender: UIButton) {
    //        publisher?.beautifulFaceExchange();
    //    }
    
    //MARK: - 提供给外部调用的方法
    //开始进行连接
    func startConnect(_ roomId:String) {
        let currentUser:OLConnectorInfos = OLConnectorInfos();
        currentUser.name = "QiaoYijie";
        currentUser.id = "123123";
//        currentUser.headImage = "https://www.baidu.com/img/bd_logo1.png";
        self.startConnect(roomId,withUser: currentUser);
    }
    
    func startConnect(_ roomId:String,withUser user:OLConnectorInfos) {
        self.roomId = roomId;
        self.user = user;
        self.backgroundColor = UIColor.black;
        // \(Config.urlHost)/service/room/get

//        let turnServerConf:[String:Any] = [
//            "turn_server":
//                jsonData["data"]["turnservers"][1].dictionaryValue,
//            "standby_relay":jsonData["data"]["standby_relay"].dictionaryValue
//        ];
        
        
        let t_url = jsonData["data"]["turnservers"][0]["url"].stringValue
        let t_username = jsonData["data"]["turnservers"][0]["username"].stringValue
        let t_credential = jsonData["data"]["turnservers"][0]["credential"].stringValue
        
        let s_url = jsonData["data"]["standby_relay"]["url"].stringValue
        let s_username = jsonData["data"]["standby_relay"]["username"].stringValue
        let s_credential = jsonData["data"]["standby_relay"]["credential"].stringValue
        
        let turnServerConf:[String:Any] = [
            "turn_server": [
                "url":                     t_url,
                "username":                t_username,
                "credential":              t_credential
            ],
            
            "standby_relay":["url":         s_url,
                             "username":    s_username,
                             "credential":  s_credential
            ]
        ]
        
        print("****************\(jsonData.rawString()!)****************")
        
        print("-=-=-=-=-===========================\(turnServerConf)  1111111")
        //352, 288
        //640, 480
        //1280, 720
        //1920, 1080
        let localVideoConf:[String:Any] = [
            "videoMaxWidth" : "352",
            "videoMaxHeight" : "288",
            "videoMaxFrameRate" : "12"
        ]
        
        
        print("\(localVideoConf)  22222222")
        //普通使用
        let connectConfig:[String:Any] = [
            //当人数达到一定数量时，再进行连接
            "realConnectWhenUserCoutUpTo" : 1,
            
            "preferVideoEncoder" : "H264",
            //直连不生效
            "minEncodingBitrate" : "30",
            //默认256
            "useBandWidth" : "512"
        ];
        
        print("\(connectConfig)  33333333")
        //直播专用
//        let connectConfig:[String:Any] = [
//            "preferVideoEncoder" : "VP8",
//            //直连不生效
//            //"minEncodingBitrate" : "10240",
//            "minEncodingBitrate" : "1024",
//            //默认256
//            "useBandWidth" : "1024"
//        ];
        
        
        //直连，一般orbit
        var sendConfig:[String:Any] = [
            "connect_room_type":jsonData["data"]["type"].stringValue,
            "node_server_host":jsonData["data"]["mediaserver"].stringValue,
            "fullScreen":true,
            "turn_server_conf":turnServerConf,
            "localVideoConf":localVideoConf,
            "connectConfig":connectConfig
        ];
        
        //老师端不需要推视频，因为学生端不需要看到老师的图像
        if Config.appType == .teacher {
            sendConfig["sendVideo"] = false
        }
        
        
        OLSession.beginSetUp();
        session = OLSession(roomId: roomId, userInfo: user, config: sendConfig, withDelegate: self, embedToView: self);

        session?.startConnect([:]);
        
        setButtonLoc(80);
        
    }
    
    //关闭界面
    func disConnect() {
        session?.disConnect();
        OLSession.endSetUp()
        session = nil;
    }
    
    //MARK: - OLSessionDelegate
    //本地视频已经初始化完毕
    func olSession(_ session: OLSession, publisherInited publisher: OLPublisher) {
        //publisher.setFrame(self.frame);
        //publisher.setSize(CGSizeMake(minVideoSize.width*2, minVideoSize.height*2));
        self.publisher = publisher;
        //MARK: - 增加一个变量控制是否全屏
        //self.publisher?.fullView = true;
        self.addSubview(voiceButton);
        self.addSubview(videoButton);
        self.addSubview(cameraChangeButton);
        
        voiceButton.isHidden = true
        videoButton.isHidden = true
        cameraChangeButton.isHidden = true
        
        addSubview(examButton_n)
        addSubview(videoButton_n)
        addSubview(voiceButton_n)
        addSubview(cameraChangeButton_n)
        
        //        self.addSubview(beautifulChangeButton);
        
        userHeadImgView = UIImageView();
        
        handleVideoViewLayout("normal");
    }

    func bringButtonsToTop() {
        if(voiceButton.superview == nil) {
            print("还没有把按钮放到页面上,无法执行bringButtonsToTop");
            return;
        }
        for i in 0..<operationButtons.count {
            self.bringSubview(toFront: operationButtons[i]);
        }
        
        for i in 0..<operationButtons_n.count {
            self.bringSubview(toFront: operationButtons_n[i]);
        }
    }
    
    func olSession(_ session: OLSession, userDidComeIn subscribes:[OLSubscribe]) {
        self.currentStatus = VideoMettingStatus.connecting;
        
        if session.roomUserInfos.count >= 2 {
            examButton_n.alive()
        } else {
            examButton_n.disable()
        }
        
        for i in 0..<subscribes.count {
            subscribes[i].setFrame(CGRect(x: -1,y: -1,width: 1,height: 1));
        }
        
        if self.publisher?.view != nil {
            
            self.bringSubview(toFront: self.publisher!.view);
        }
        
        self.bringSubview(toFront: self.userHeadImgView!);
        
        handleVideoViewLayout("normal");
        self.bringButtonsToTop();
        
        
        print("=============================start::userDidComeInuserDidComeInuserDidComeIn::=============================================");
        for info in session.roomUserInfos {
            print("\(info.name)::\(info.uuid)::\(info.id)");
        }
        print("=============================合计人数:\(session.roomUserInfos.count)=============================================");
        print("=============================end::userDidComeInuserDidComeInuserDidComeIn::=============================================");
        
        if let action = userComeInBlock {
            action()
        }
    }
    
    //用户将要连接上了
    func olSession(_ session: OLSession, userWillComming userInfo: OLConnectorInfos) {
        print("userInfo.uuid = \(userInfo.uuid)");
        appendUserInfo(userInfo);
        
        if session.roomUserInfos.count >= 2 {
            examButton_n.alive()
        } else {
            examButton_n.disable()
        }
        
        print("=============================start::有人进入::=============================================");
        if let userInfos = self.session?.roomUserInfos {
            for info in userInfos {
                print("\(info.name)::\(info.uuid)::\(info.id)");
            }
            print("=============================合计人数:\(userInfos.count)=============================================");
        }
        print("=============================end::有人进入::=============================================");
        
        handleVideoViewLayout("normal");
    }
    //当有人离开时
    func olSessionConnectorLeave(_ connector:OLConnectorInfos) {
        
        for i in 0..<videoUsers.count {
            if connector.uuid == videoUsers[i].uuid {
                videoUsers.remove(at: i);
                break;
            }
        }
        
        if session?.roomUserInfos.count ?? 0 >= 2 {
            examButton_n.alive()
        } else {
            examButton_n.disable()
        }
        print("离开人的UUID：\(connector.uuid)")
        
        print("=============================start::有人离开::=============================================");
        if let userInfos = session?.roomUserInfos {
            for info in userInfos {
                print("\(info.name)::\(info.uuid)::\(info.id)");
            }
            print("=============================合计人数:\(userInfos.count)=============================================");
        }
        print("=============================end::有人离开::=============================================");
//        handleVideoViewLayout("normal");

    }
    //TODO 有可能被重复添加了，在userWillComming的时候，看看是哪里导致的
    func olSessionInitedUser(_ userInfo: [OLConnectorInfos]) {
        for i in 0..<userInfo.count {
            
            if userInfo[i].uuid == self.session?.uuid {
                continue;
            }
            
            appendUserInfo(userInfo[i]);
        }
        
        print("=============================start::还没进房间就有人在::=============================================");
        if let userInfos = session?.roomUserInfos {
            for info in userInfos {
                print("\(info.name)::\(info.uuid)::\(info.id)");
            }
        print("=============================合计人数:\(userInfos.count)=============================================");
        }
        print("=============================end::还没进房间就有人在::=============================================");
    }
    
    fileprivate func appendUserInfo(_ userInfo:OLConnectorInfos) {
        for i in 0..<videoUsers.count {
            if videoUsers[i].uuid == userInfo.uuid {
                return;
            }
        }
        videoUsers.append(userInfo);
    }
    
    func olSession(_ session: OLSession, statedChange changeData: [String : Any]) {
        if let mute = changeData["mute"] as? Bool {
            /*
             if(mute == true) {
             enableVoice = false;
             } else {
             enableVoice = true;
             }*/
            /*
             if publisher!.isMute != mute {
             session.changeSoundType(mute);
             changeSlienceButtonType();
             }*/
            changeSlienceButtonType();
        }
    }
    
    func olSessionReleasePeer() {
        handleVideoViewLayout("normal");
    }
    
    func olSessionTracksChanged(_ session: OLSession) {
        handleVideoViewLayout("normal");
    }
    
    //MARK: - 兼容获取头像，后面会增加额外的字段
    func filterName(_ userInfo:OLConnectorInfos) -> OLConnectorInfos {
        let newUserInfo = OLConnectorInfos();
        newUserInfo.uid = userInfo.uid;
        newUserInfo.uuid = userInfo.uuid;
        
        let jsonName = userInfo.name;
        newUserInfo.name = userInfo.name;
        
        let (jsonData,error) = jsonName.toJsonObject();
        
        if(error == nil && jsonData != nil) {
            if let name = jsonData!.object(forKey: "name") as? String {
                newUserInfo.name = name;
            }
        }
        return newUserInfo;
    }
    
    func olSessionState(_ resultDictionary: [String : String], subscribe: OLSubscribe?) {
        
    }
    
    //根据数量显示video view
    func handleVideoViewLayout(_ status: String) {
        if(status == "normal"){
            fullScreenViewHandler(false);
        }else{
            fullScreenLeaveSceneHandler();
        }
        
    }
    fileprivate func miniScreenViewHandler(){
        if let count = session?.subscribes.count {
            if(count == 0){
                publisher?.setFrame(CGRect(x: 0, y: 0, width: MINI_VIDEO_WIDTH, height: MINI_VIDEO_HEIGHT));
                userHeadImgView?.frame = CGRect(x: 0, y: 0, width: MINI_VIDEO_WIDTH, height: MINI_VIDEO_HEIGHT);
                return;
            }
            
            var publisherX: CGFloat = 0.0;
            var publisherY: CGFloat = 0.0;
            
            if(count == 1){
                publisherY = 45.0;
            }else if(count == 2){
                publisherX = 45.0;
            }
            
            publisher?.setFrame(CGRect(x: publisherX, y: publisherY, width: MINI_VIDEO_WIDTH / 2.0, height: MINI_VIDEO_HEIGHT / 2.0));
            userHeadImgView?.frame = CGRect(x: publisherX, y: publisherY, width: MINI_VIDEO_WIDTH / 2.0, height: MINI_VIDEO_HEIGHT / 2.0);
            
            for i in 0 ..< count {
                switch(count){
                case 1:
                    session?.subscribes[i].setFrame(CGRect(x:MINI_VIDEO_WIDTH / 2.0, y:45.0, width:MINI_VIDEO_WIDTH / 2.0, height: MINI_VIDEO_HEIGHT / 2.0));
                    break;
                case 2:
                    if(i == 0){
                        session?.subscribes[i].setFrame(CGRect(x:0, y:MINI_VIDEO_WIDTH / 2.0, width:MINI_VIDEO_WIDTH / 2.0, height: MINI_VIDEO_HEIGHT / 2.0));
                    }else{
                        session?.subscribes[i].setFrame(CGRect(x:MINI_VIDEO_WIDTH / 2.0, y:MINI_VIDEO_WIDTH / 2.0, width:MINI_VIDEO_WIDTH / 2.0, height:MINI_VIDEO_HEIGHT / 2.0));
                    }
                    break;
                default:
                    if(i == 0){
                        session?.subscribes[i].setFrame(CGRect(x:MINI_VIDEO_WIDTH / 2.0, y:0, width:MINI_VIDEO_WIDTH / 2.0, height:MINI_VIDEO_HEIGHT / 2.0));
                    }else if(i == 1){
                        session?.subscribes[i].setFrame(CGRect(x:0, y:MINI_VIDEO_WIDTH / 2.0, width:MINI_VIDEO_WIDTH / 2.0, height:MINI_VIDEO_HEIGHT / 2.0));
                    }else{
                        session?.subscribes[i].setFrame(CGRect(x:MINI_VIDEO_WIDTH / 2.0, y:MINI_VIDEO_WIDTH / 2.0, width:MINI_VIDEO_WIDTH / 2.0, height:MINI_VIDEO_HEIGHT / 2.0));
                    }
                }
            }
        }else{
            publisher?.setFrame(CGRect(x: 0, y: 0, width: MINI_VIDEO_WIDTH, height: MINI_VIDEO_HEIGHT));
            userHeadImgView?.frame = CGRect(x: 0, y: 0, width: MINI_VIDEO_WIDTH, height: MINI_VIDEO_HEIGHT);
        }
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kVideoViewFramWillChangeNotification), object: nil);
    }
    
    fileprivate func fullScreenViewHandler(_ fromNotifcation: Bool){
        switch(currentStatus) {
        case .waiting :
            //MARK: - 显示全屏标志位
            //publisher?.setSize(CGSizeMake(minVideoSize.width*2, minVideoSize.height*2));
            publisher?.setSize(UIScreen.main.bounds.size);
            publisher?.setCenter(self.center);
            break;
        case .connecting :
            let mainBounds = UIScreen.main.bounds;
            let viewCenter = CGPoint(x: mainBounds.width/2, y: mainBounds.height/2);
            if let count = session?.roomUserInfos.count {
                print("count------:\(count)")
                switch(count) {
                case 2:
                    guard session?.subscribes.count == 1 else {
                        return;
                    }
                    //let firstSub = (session?.subscribes.count != session?.roomUserInfos.count) ? session?.subscribes[1] : session?.subscribes[0];
                    let firstSub = session?.subscribes[0];
                    firstSub?.setSize(self.frame.size);
                    //firstSub?.setSize(CGSize(width: 400, height: 120));
                    //firstSub?.setSize(CGSize(width: 145, height: 155.4));
                    //firstSub?.setSize(CGSize(width: 200, height: 200));
                    firstSub?.setCenter(viewCenter);
                    
                    
                    if Config.appType == .teacher {
                        publisher?.setSize(CGSize(width: MINI_VIDEO_WIDTH, height: MINI_VIDEO_HEIGHT));
                        publisher?.setCenter(CGPoint(x: viewCenter.x + minVideoSize.width - minVideoSize.width/3,  y: viewCenter.y - minVideoSize.height + minVideoSize.height/3 - 20));
                    } else {
                        XBHHUD.showSuccess("老师已经进入房间")
                    }
                    break;
                case 3:
                    guard session?.subscribes.count == 2 else {
                        return;
                    }
                    publisher?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                    
                    //let firstSub = (session?.subscribes.count != session?.roomUserInfos.count) ? session?.subscribes[1] : session?.subscribes[0];
                    let firstSub = session?.subscribes[0];
                    firstSub?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                    
                    //let secondSub = (session?.subscribes.count != session?.roomUserInfos.count) ? session?.subscribes[2] : session?.subscribes[1];
                    let secondSub = session?.subscribes[1];
                    secondSub?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                    
                    publisher?.setCenter(CGPoint(x: viewCenter.x, y: viewCenter.y-minVideoSize.height*0.5));
                    firstSub?.setCenter(CGPoint(x: viewCenter.x*0.5, y: viewCenter.y+minVideoSize.height*0.5));
                    secondSub?.setCenter(CGPoint(x: viewCenter.x*1.5, y: viewCenter.y+minVideoSize.height*0.5));
                    
                    break;
                case 4:
                    guard session?.subscribes.count == 3 else {
                        return;
                    }
                    publisher?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                    
                    //let firstSub = (session?.subscribes.count != session?.roomUserInfos.count) ? session?.subscribes[1] : session?.subscribes[0];
                    let firstSub = session?.subscribes[0];
                    firstSub?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                    
                    //let secondSub = (session?.subscribes.count != session?.roomUserInfos.count) ? session?.subscribes[2] : session?.subscribes[1];
                    let secondSub = session?.subscribes[1];
                    secondSub?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                    
                    //let thirdSub = (session?.subscribes.count != session?.roomUserInfos.count) ? session?.subscribes[3] : session?.subscribes[2];
                    let thirdSub = session?.subscribes[2];
                    thirdSub?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                    
                    publisher?.setCenter(CGPoint(x: viewCenter.x*0.5, y: viewCenter.y-minVideoSize.height*0.5));
                    firstSub?.setCenter(CGPoint(x: viewCenter.x*1.5, y: viewCenter.y-minVideoSize.height*0.5));
                    secondSub?.setCenter(CGPoint(x: viewCenter.x*0.5, y: viewCenter.y+minVideoSize.height*0.5));
                    thirdSub?.setCenter(CGPoint(x: viewCenter.x*1.5, y: viewCenter.y+minVideoSize.height*0.5));
                    break;
//                case 7:
//                    publisher?.setSize(CGSize(width: MINI_VIDEO_WIDTH, height: MINI_VIDEO_HEIGHT));
//                    for i in 0..<session!.subscribes.count {
//                        session!.subscribes[i].setFrame(CGRect(x:10+i*110,y:200,width:100,height:100));
//                    }
//                    break;
                default :
                    print("count:\(count)")
                    //MARK: - 显示全屏标志位
                    //publisher?.setSize(UIScreen.mainScreen().bounds.size);
                    //publisher?.setCenter(self.center);
                    publisher?.setFrame(UIScreen.main.bounds);
                }
                break;
            }
            if let refFrame = publisher?.view.frame {
                userHeadImgView?.frame = refFrame;
            }
            }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kVideoViewFramWillChangeNotification), object: nil);
        
    }
    
    fileprivate func fullScreenLeaveSceneHandler(){
        if let count = session?.subscribes.count {
            let mainBounds = UIScreen.main.bounds;
            let viewCenter = CGPoint(x: mainBounds.width/2, y: mainBounds.height/2);
            
            switch(count) {
            case 1:
                let firstSub = session?.subscribes[0];
                firstSub?.setSize(CGSize(width: minVideoSize.width*2, height: minVideoSize.height*2));
                firstSub?.setCenter(viewCenter);
                
                publisher?.setSize(CGSize(width: minVideoSize.width/1.5, height: minVideoSize.height/1.5));
                
                UIView.animate(withDuration: 0.3, animations: { [weak self]() -> Void in
                    if(self != nil) {
                        firstSub?.setSize(CGSize(width: self!.minVideoSize.width*2, height: self!.minVideoSize.height*2));
                        firstSub?.setCenter(viewCenter);
                        
                        self!.publisher?.setCenter(CGPoint(x: viewCenter.x + self!.minVideoSize.width - self!.minVideoSize.width/3,  y: viewCenter.y - self!.minVideoSize.height + self!.minVideoSize.height/3));
                    }
                    }, completion: nil);
                
                break;
            case 2:
                publisher?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                
                let firstSub = session?.subscribes[0];
                firstSub?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                
                let secondSub = session?.subscribes[1];
                secondSub?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                
                UIView.animate(withDuration: 0.3, animations: { [weak self]() -> Void in
                    if(self != nil) {
                        self!.publisher?.setCenter(CGPoint(x: viewCenter.x, y: viewCenter.y-self!.minVideoSize.height*0.5));
                        firstSub?.setCenter(CGPoint(x: viewCenter.x*0.5, y: viewCenter.y+self!.minVideoSize.height*0.5));
                        secondSub?.setCenter(CGPoint(x: viewCenter.x*1.5, y: viewCenter.y+self!.minVideoSize.height*0.5));
                    }
                    }, completion: nil);
                
                break;
            case 3:
                publisher?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                
                let firstSub = session?.subscribes[0];
                firstSub?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                
                let secondSub = session?.subscribes[1];
                secondSub?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                
                let thirdSub = session?.subscribes[2];
                thirdSub?.setSize(CGSize(width: minVideoSize.width, height: minVideoSize.height));
                
                UIView.animate(withDuration: 0.3, animations: { [weak self]() -> Void in
                    if(self != nil) {
                        self!.publisher?.setCenter(CGPoint(x: viewCenter.x*0.5, y: viewCenter.y-self!.minVideoSize.height*0.5));
                        firstSub?.setCenter(CGPoint(x: viewCenter.x*1.5, y: viewCenter.y-self!.minVideoSize.height*0.5));
                        secondSub?.setCenter(CGPoint(x: viewCenter.x*0.5, y: viewCenter.y+self!.minVideoSize.height*0.5));
                        thirdSub?.setCenter(CGPoint(x: viewCenter.x*1.5, y: viewCenter.y+self!.minVideoSize.height*0.5));
                    }
                    }, completion: nil);
                
                break;
            default :
                //MARK: - 显示全屏标志位
                //publisher?.setSize(CGSizeMake(minVideoSize.width*2, minVideoSize.height*2));
                publisher?.setSize(UIScreen.main.bounds.size);
                
                UIView.animate(withDuration: 0.3, animations: {[weak self]() -> Void in
                    if(self != nil) {
                        self!.publisher?.setCenter(viewCenter);
                    }
                    }, completion: nil);
            }
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: kVideoViewFramWillChangeNotification), object: nil);
        
    }
    
    func setButtonLoc(_ buttonY:CGFloat) {
        let buttonX:CGFloat = 10;
        let buttonHeight = voiceButton.frame.size.height;
        voiceButton.frame.origin = CGPoint(x: buttonX, y: buttonY);
        videoButton.frame.origin = CGPoint(x: buttonX, y: buttonY+buttonHeight+10);
        cameraChangeButton.frame.origin = CGPoint(x: buttonX, y: buttonY+buttonHeight*2+20);
        //screenCutButton.frame.origin = CGPointMake(buttonX, buttonY+buttonHeight*3+30);
        
        let margin:Double = Double((UIScreen.main.bounds.width - examButton_n.width * 4) / 5)
        let minY:Double = Double(UIScreen.main.bounds.height - 150)
        let buttonW:Double = Double(examButton_n.width)
        
        examButton_n.frame.origin = CGPoint(x: margin, y: minY)
        voiceButton_n.frame.origin = CGPoint(x: margin * 2.0 + buttonW, y: minY)
        videoButton_n.frame.origin = CGPoint(x: margin * 3 + buttonW * 2, y: minY)
        cameraChangeButton_n.frame.origin = CGPoint(x: margin * 4 + buttonW * 3, y: minY)
        
        if Config.appType == .teacher {
            //老师端
            let margin:Double = Double((UIScreen.main.bounds.width - examButton_n.width * 2) / 3)
            let minY:Double = Double(UIScreen.main.bounds.height - 150)
            let buttonW:Double = Double(examButton_n.width)
            
            examButton_n.frame.origin = CGPoint(x: margin, y: minY)
            voiceButton_n.frame.origin = CGPoint(x: margin * 2.0 + buttonW, y: minY)
            videoButton_n.isHidden = true
            cameraChangeButton_n.isHidden = true
        } else {
            //学生端
            let margin:Double = Double((UIScreen.main.bounds.width - examButton_n.width * 3) / 4)
            let minY:Double = Double(UIScreen.main.bounds.height - 150)
            let buttonW:Double = Double(examButton_n.width)
            
            examButton_n.isHidden = true
            voiceButton_n.isHidden = true
            videoButton_n.isHidden = true
            cameraChangeButton_n.frame.origin = CGPoint(x: margin * 2 + buttonW, y: minY + 23)
            
        }
        
        
    }
    
    func publisherMoveUp() {
        if let count = session?.subscribes.count {
            if(count == 1) {
                let mainBounds = UIScreen.main.bounds;
                let viewCenter = CGPoint(x: mainBounds.width/2, y: mainBounds.height/2);
                UIView.animate(withDuration: 0.3, animations: { [weak self]() -> Void in
                    if(self != nil) {
                        self!.publisher?.setCenter(CGPoint(x: viewCenter.x + self!.minVideoSize.width - self!.minVideoSize.width/3,  y: viewCenter.y - self!.minVideoSize.height - 20));
                        self!.userHeadImgView?.center = CGPoint(x: viewCenter.x + self!.minVideoSize.width - self!.minVideoSize.width/3,  y: viewCenter.y - self!.minVideoSize.height - 20);
                    }
                    }, completion: nil);
                
            }
        }
    }
    
    func publisherMoveDown() {
        if let count = session?.subscribes.count {
            if(count == 1) {
                let mainBounds = UIScreen.main.bounds;
                let viewCenter = CGPoint(x: mainBounds.width/2, y: mainBounds.height/2);
                UIView.animate(withDuration: 0.3, animations: { [weak self]() -> Void in
                    if(self != nil) {
                        self!.publisher?.setCenter(CGPoint(x: viewCenter.x + self!.minVideoSize.width - self!.minVideoSize.width/3,  y: viewCenter.y - self!.minVideoSize.height + self!.minVideoSize.height/3 - 20));
                        self!.userHeadImgView?.center = CGPoint(x: viewCenter.x + self!.minVideoSize.width - self!.minVideoSize.width/3,  y: viewCenter.y - self!.minVideoSize.height + self!.minVideoSize.height/3 - 20);
                    }
                    }, completion: nil);
            }
        }
    }
    
    //音频设备被占用
    func leaveForReasonAudio() {
        session?.sendTip("对方音频被占用");
    }
    
    //直播用
    func olSessionCanStartBroadcast(_ session: OLSession) {
        session.startBroadcast();
    }
}

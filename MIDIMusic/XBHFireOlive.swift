//
//  XBHFireOlive.swift
//  FireOlive_iOS
//
//  Created by QiaoYijie on 17/3/1.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit

protocol XBHFireOliveDelegate: NSObjectProtocol {
    func fireOlivedidReceivePushMessage(message: String)
}

class XBHFireOlive: NSObject {
    
    var host = ""
    var path = ""
    var transport: XBHTransport?
    
    weak var delegate: XBHFireOliveDelegate?
    
    // 用此方法初始化
    convenience init?(url: String, baseHost: String) {
        self.init()
        
        if let (path, host) = analyseUrl(url: url, baseHost: baseHost) {
            self.host = host
            self.path = path
            self.transport = XBHTransportManager.shareInstance.connect(host: host, fireOlive: self)
            getAllMessage(message: ["action" : "getAll", "path": self.path])
        } else {
            XBHHUD.showError("FireOlive get error url: \(url)")
            return nil
        }
    }
    
    // 发送消息
    fileprivate func sendMessage(message: [String: Any], type: MessageType) {
        
        var sendData = [String: Any]()
        
        //选择消息的类型
            sendData = [
            "action" : type.rawValue,
            "message" : message,
            "path" : self.path,
            ]
        
        transport?.sendMessage(message: sendData)
    }
    
    func setMessage(message: [String: Any]) {
        sendMessage(message: message, type: .set)
    }
    
    func pushMessage(message: [String: Any]) {
        sendMessage(message: message, type: .push)
    }
    
    fileprivate func getAllMessage(message: [String: Any]) {
        sendMessage(message: message, type: .getAll)
    }
    
    // 解析地址
    fileprivate func analyseUrl(url: String, baseHost: String) -> (path: String, host: String)? {
        
        let socketHost = baseHost + "?db="
        let data: [String] = url.split("/")
        
        if data.count < 4 {
            return nil
        }
        
        let DBName = data[1]
        let path = "\(data[2])/\(data[3])"
        let host = "\(socketHost)\(DBName)"
        
        return (path, host)
    }
    
    func close() {
        transport?.close()
        _ = transport?.fireOliveArray.removeValue(forKey: self.path)
    }
    
    // MARK: - transportDelegate
    
    // 收到了消息
    func transportDidReceiveMessage(message: String, type: MessageType) {
        if type == .push {
            self.delegate?.fireOlivedidReceivePushMessage(message: message)
        } else if type == .set {
            
        } else if type == .getAll { }
    }
    
    deinit {
        Print.dlog("XBHFireOlive deinit")
    }
}

//
//  JSQMessagesIncomingShareCell.h
//  interview
//
//  Created by 边宁 on 16/2/1.
//  Copyright © 2016年 orangelab. All rights reserved.
//

#import "JSQMessagesCollectionViewCell.h"

@interface JSQMessagesIncomingShareCell : JSQMessagesCollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *Icon;

@end

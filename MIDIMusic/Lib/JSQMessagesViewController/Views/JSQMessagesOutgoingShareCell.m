//
//  JSQMessagesOutgoingShareCell.m
//  interview
//
//  Created by 边宁 on 16/2/1.
//  Copyright © 2016年 orangelab. All rights reserved.
//

#import "JSQMessagesOutgoingShareCell.h"

@implementation JSQMessagesOutgoingShareCell
+ (UINib *)nib
{
    return [UINib nibWithNibName:NSStringFromClass([JSQMessagesOutgoingShareCell class])
                          bundle:[NSBundle mainBundle]];
}

+ (NSString *)cellReuseIdentifier
{
    return NSStringFromClass([JSQMessagesOutgoingShareCell class]);
}

#pragma mark - Initialization

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.messageBubbleTopLabel.textAlignment = NSTextAlignmentRight;
    self.cellBottomLabel.textAlignment = NSTextAlignmentRight;
}

- (void)setType:(NSString *)type {
    
    if ([type isEqualToString:@"msg"]) {
        return;
    }
    
    if ([type isEqualToString:@"url"]) {
        _Icon.image = [UIImage imageNamed:@"聊天室显示网页分享88 106"];
    } else if ([type isEqualToString:@"whiteboard"]) {
         _Icon.image = [UIImage imageNamed:@"聊天室显示白板分享106 106"];
    } else if ([type isEqualToString:@"picture"]) {
        _Icon.image = [UIImage imageNamed:@"聊天室显示图片共享"];
    } else {
        _Icon.image = [UIImage imageNamed:@"70 78"];
    }
}

@end

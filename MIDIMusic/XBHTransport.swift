//
//  XBHTransport.swift
//  FireOlive_iOS
//
//  Created by QiaoYijie on 17/3/1.
//  Copyright © 2017年 orangelab. All rights reserved.
//

import UIKit
import SwiftyJSON

class XBHTransport: NSObject {
    
    var webSocket = SRWebSocket()
    var host = ""
    var fireOliveArray = [String: XBHFireOlive]() {
        didSet {
            if fireOliveArray.count == 0 {
                self.close()
            }
        }
    }
    var messageArray = [[String: Any]]()
    
    //心跳检测定时器
    var timer = Timer()
    
    // 通过此方法初始化
    init(host: String) {
        super.init()
        self.host = host
        reconnect()
    }
    
    func reconnect() {
        webSocket.delegate = nil
        webSocket = SRWebSocket(url: URL(string: host))
        webSocket.delegate = self
        webSocket.open()
        
    }
    
    func sendMessage(message: [String: Any]) {
        
        var sendData = message
        
        sendData["messageUUID"] = Utils.getRandomStringOfLength(length: 10)
        sendData["version"] = "v06.12.12"
        
        messageArray.append(sendData)
        
        if let sendString = Utils.dicToJSONStr(sendData as NSDictionary) {
            Print.dlog("sended message: \(sendString)")
            webSocket.send(sendString)
        }
    }
    
    func close() {
        timer.invalidate()
        webSocket.delegate = nil
        webSocket.close()
        XBHTransportManager.shareInstance.transportArray.removeValue(forKey: host)
    }
    
    deinit {
        Print.dlog("XBHTransport销毁了 host:\(host)")
    }
}


extension XBHTransport: SRWebSocketDelegate {
    
    //MARK: - SRWebSocketDelegate
    func webSocketDidOpen(_ webSocket: SRWebSocket!) {
        Print.dlog("webSocketDidOpen")
        
        for message in messageArray {
            Print.dlog("\(messageArray.count)")
            if let sendString = Utils.dicToJSONStr(message as NSDictionary) {
                Print.dlog("sended message: \(sendString)")
                webSocket.send(sendString)
            }
        }
        
        //开始心跳检测
        if #available(iOS 10.0, *) {
            timer = Timer.scheduledTimer(withTimeInterval: 15.0, repeats: true, block: { [weak self] (timer) in
                self?.webSocket.send("2")
            })
        } else {
            // Fallback on earlier versions
        }
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didReceiveMessage message: Any!) {
        
        guard let jsonString = message as? String else { return }
        Print.dlog("didReceiveMessage \(jsonString))")
        
        if let dataFromString = jsonString.data(using: .utf8, allowLossyConversion: false) {
            let messageData = JSON(data: dataFromString)
            
//            Print.dlog("\(messageData["action"].stringValue)")
            if messageData["action"].stringValue == "dataReceive" {
                for (index, message) in messageArray.enumerated() {
                    if message["messageUUID"] as? String ?? "none" == messageData["data"].stringValue {
                        messageArray.remove(at: index)
                    }
                }
                return
            }
            
            if !messageData["key"].stringValue.isEmpty && !messageData["key"].stringValue.isEmpty {
                
                Print.dlog("\(messageData["key"].stringValue)  \(messageData["table"].stringValue)")
                
                let fireOlivePath = messageData["table"].stringValue + "/" + messageData["key"].stringValue
                
                let fireOlive = fireOliveArray[fireOlivePath]
                fireOlive?.transportDidReceiveMessage(message: jsonString, type: MessageType.getMessageType(type: messageData["action"].stringValue))
            }
        }
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didFailWithError error: Error!) {
        Print.dlog("didFailWithError")
        reconnect()
    }
    
    func webSocket(_ webSocket: SRWebSocket!, didCloseWithCode code: Int, reason: String!, wasClean: Bool) {
        Print.dlog("didCloseWithCode")
        reconnect()
    }
}

//socketIO
extension XBHTransport {
    
}

//
//  ViewController.swift
//  MIDIMusic
//
//  Created by QiaoYijie on 16/11/17.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import AVKit

class ViewController: UIViewController,UITextFieldDelegate {
    
    @IBOutlet weak var MeetingNumberLabel: HoshiTextField!
    @IBOutlet weak var enterButton: UIButton!
    
    var lookBackPlayer = AVQueuePlayer()
    
    var fireOlivePath = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.navigationBar.isTranslucent = false
        
        switch Config.appType {
        case .teacher:
            title = "迷笛(老师端)"
        case .student:
            title = "迷笛(学生端)"
        }
    }
    
    
    @IBAction func lookBack(_ sender: Any) {
        GlobalVar.currentRoomID = MeetingNumberLabel.text ?? ""
        //观看回放
        XBHHUD.showLoading()
        
        OrangeLab.shared.getlookBackUrls(roomID: GlobalVar.currentRoomID) { (response) in
            XBHHUD.hide()
            
            guard let lookBackUrls = response["lookback_url"] as? [String] else {
                XBHHUD.showError("请检查您的网络")
                return
            }
            
            var avitemArr = [AVPlayerItem]()
            
            for item in lookBackUrls {
                let url = URL(string: item)
                let avItem = AVPlayerItem(url: url!)
                avitemArr.append(avItem)
            }
            
            self.lookBackPlayer = AVQueuePlayer.init(items: avitemArr)
            
            Utils.runInMainThread {
                let playerController = LookBackViewController()
                playerController.player = self.lookBackPlayer
                self.navigationController?.pushViewController(playerController, animated: true)
            }
            
            
        }
        
        
    }
    
    @IBAction func createRoomClick(_ sender: Any) {
        
        GlobalVar.currentRoomID = MeetingNumberLabel.text ?? ""
        //创建房间
        XBHHUD.showLoading()
        
        
        
        OrangeLab.shared.createRoom {(response) in
            XBHHUD.hide()
            
            guard let pushUrl = response["push_url"] as? String else {
                XBHHUD.showError("请检查您的网络")
                return;
            }
            GlobalVar.pushUrl = pushUrl
            
            guard let roomID = response["room_name"] as? String else {
                XBHHUD.showError("请检查您的网络")
                return;
            }
            GlobalVar.currentRoomID = roomID
            Utils.runInMainThread {
                self.MeetingNumberLabel.text = roomID
            }
            
            guard let watchDic = response["play_urls"] as? [String: Any] else {
                XBHHUD.showError("请检查您的网络")
                return;
            }
            
            guard let watchArr = watchDic["rtmpUrls"] as? [Any] else {
//            guard let watchArr = watchDic["hlsUrls"] as? [Any] else {
                XBHHUD.showError("请检查您的网络")
                return;
            }
            
            
            guard let watchUrl = watchArr[0] as? String else {
                XBHHUD.showError("请检查您的网络")
                return
            }
            GlobalVar.watchUrl = watchUrl
        }
        
    }
    
    @IBAction func enterClicked(_ sender: Any) {
        if MeetingNumberLabel.text == "" {
            XBHHUD.showError("请输入考试码")
            return
        }
        GlobalVar.currentRoomID = MeetingNumberLabel.text ?? ""
        
        guard let token = UserDefaults.standard.object(forKey: "appAccessToken") as? String else {
            return;
        }
        
        XBHHUD.showLoading()
        Alamofire.request("https://open.intviu.cn:8053/v1/service/room/get", method: .post, parameters: ["room_name":"\(GlobalVar.currentRoomID)"], encoding: JSONEncoding.default, headers:  ["X-Api-Token":token, "cid": "\(UIDevice.current.identifierForVendor?.uuidString.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed) ?? "")"]).responseData { (response) in
            
            switch response.result {
            case .success(let value):
                let json = JSON(data: value)
                
                
                let resString = String(data: response.result.value!, encoding: String.Encoding.utf8)
                print(resString!)
                
                if json["code"].intValue == 0 {
                    
                    
                    //开始接收数据
                    let roomInfo = RoomInfo()
                    roomInfo.roomId = self.MeetingNumberLabel.text ?? ""
                    
                    if json["data"]["msg_server"]["type"] == "fireolive" {
                        self.fireOlivePath = json["data"]["msg_server"]["server"].stringValue
                    }
                    
                    let c_user = User()
                    c_user.id = "123123"
                    c_user.name = "QiaoYijie"
                    
//                    fireBaseChatLoader.startPostNotification(roomInfo, currentUser: c_user, fireOlivePath: self.fireOlivePath)
                    
                    
                    
                    OrangeLab.shared.getPushOrWarchAddress(roomID: GlobalVar.currentRoomID) { (response) in
                        XBHHUD.hide()
                        
                        guard let pushUrl = response["push_url"] as? String else {
                            XBHHUD.showError("请检查您的网络")
                            return;
                        }
                        GlobalVar.pushUrl = pushUrl
                        
                        guard let roomID = response["room_name"] as? String else {
                            XBHHUD.showError("请检查您的网络")
                            return;
                        }
                        GlobalVar.currentRoomID = roomID
                        Utils.runInMainThread {
                            self.MeetingNumberLabel.text = roomID
                        }
                        
                        guard let watchDic = response["play_urls"] as? [String: Any] else {
                            XBHHUD.showError("请检查您的网络")
                            return;
                        }
                        
                        guard let watchArr = watchDic["rtmpUrls"] as? [Any] else {
//                        guard let watchArr = watchDic["hlsUrls"] as? [Any] else {
                            XBHHUD.showError("请检查您的网络")
                            return;
                        }
                        
                        
                        guard let watchUrl = watchArr[0] as? String else {
                            XBHHUD.showError("请检查您的网络")
                            return
                        }
                        GlobalVar.watchUrl = watchUrl
                        
                        Utils.runInMainThread {
                            
                            let videoController = VideoViewController()
                            videoController.roomId = self.MeetingNumberLabel.text ?? ""
                            videoController.jsonData = json
                            videoController.fireOlivePath = self.fireOlivePath
                            self.navigationController?.pushViewController(videoController, animated: true)
                        }
                        
                    }
                    
                    
                } else {
                    XBHHUD.showError(json["message"].stringValue)
                }
                
            case .failure:
                XBHHUD.showError("请检查您的网络")
            }
        }
    }
    
    //MARK:- textFieldDelegate
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        fireBaseChatLoader.clearRoomChat()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true)
    }
}


//
//  BaseViewController.swift
//  MIDIMusic
//
//  Created by QiaoYijie on 16/11/19.
//  Copyright © 2016年 orangelab. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        switch Config.appType {
        case .teacher:
            title = "迷笛(老师端)"
        case .student:
            title = "迷笛(学生端)"
        }
        
        
        let leftBar = UIBarButtonItem(title: "返回", style: UIBarButtonItemStyle.plain, target: self, action: #selector(WatchController.back))
        self.navigationItem.leftBarButtonItem = leftBar
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func back() {
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
